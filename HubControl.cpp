#include "HubControl.h"

const char HubControl::COMMAND_HEADER[4] = {'!', '!', '!', '!'};

/**
 * @brief Request the time from the RasPi over UART
 * @return time_t of the current time
 */
time_t HubControl::request_time() {
    bool timeout = false;
    union {
        time_t _union_time;
        uint8_t _union_bytes[sizeof(time_t)];
    };

    flush_serial_buffer();
    Serial.print(this->COMMAND_HEADER);
    Serial.write(HubCommand::RequestTime);
    Serial.println();
    Serial.flush();
    unsigned long start_time = millis();

    for (uint8_t i = 0; i < sizeof(time_t) && i < sizeof(_union_bytes); i++) {
        while (Serial.available() == 0 && millis() - start_time < UART_TIMEOUT)
            ; // Busy wait or timeout
        if (millis() - start_time >= UART_TIMEOUT)
            timeout = true;
        _union_bytes[i] = Serial.read();
    }

    if (timeout) {
        Serial.println(TIMEOUT_WARNING);
        delay(10);
    }

    return _union_time;
}

/**
 * @brief Validate if a char is a valid HubCommand
 * @param[in] cmd: character to check
 * @return True if the character is a valid command
 */
bool HubControl::valid_hub_command(char cmd) {
    Serial.println(F("Validating cmd "));
    Serial.print(cmd);
    switch ((HubCommand) cmd) {
        case HubCommand::SendAdjTXPwr:
        case HubCommand::SendAdjSamp:
            return true;
        default:
            return false;
    }
}

/**
 * @brief Read UART traffic (5 bytes) and determine what kind of command it is
 * @return The type of command as a HubCommand
 */
HubCommand HubControl::check_command_type() {
    uint8_t in_char;
    uint8_t *_buf = (uint8_t *) calloc(16, sizeof(uint8_t));

    for (uint8_t i = 0; i < sizeof(this->COMMAND_HEADER); i++) {
        while (Serial.available() == 0)
            ; // Busy wait
        _buf[i] = Serial.read();
        if (_buf[i] != this->COMMAND_HEADER[i]) { // Checking the command header
            Serial.print(F("Header invalid: "));
            Serial.println((char) _buf[i]);
            Serial.println(this->COMMAND_HEADER[i]);
            free(_buf);
            return HubCommand::INVALID;
        }
    }

    while (Serial.available() == 0)
        ; // Busy wait
    in_char = Serial.read(); // Grab the actual command

    if (this->valid_hub_command((char) in_char)) {
        free(_buf);
        return (HubCommand) in_char;
    }

    Serial.println(F("Command invalid"));
    free(_buf);
    return HubCommand::INVALID;
}

/**
 * @brief Read a SendAdjTXPwr command from the RasPi over UART
 * @param[out] dest_node_id: The destination node id
 * @param[out] tx_pwr: The commanded TX power
 */
void HubControl::read_adj_tx_pwr_cmd(uint8_t *dest_node_id, int8_t *tx_pwr) {
    bool timeout = false;
    uint8_t memsize =
            sizeof(uint8_t) +
            sizeof(int8_t);
    uint8_t *_buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));

    unsigned long start_time = millis();

    for (uint8_t i = 0; i < memsize; i++) {
        while (Serial.available() == 0 && millis() - start_time < UART_TIMEOUT)
            ; // Busy wait or timeout
        if (millis() - start_time >= UART_TIMEOUT)
            timeout = true;
        _buf[i] = Serial.read();
    }

    *dest_node_id = _buf[0];
    *tx_pwr = _buf[1];

    free(_buf);

    if (timeout) {
        Serial.println(TIMEOUT_WARNING);
        delay(10);
    }
}

/**
 * @brief Read a SendAdjSamp command from the RasPi over UART
 * @param[in] dest_node_id: The destination node id
 * @param[in] samp_rate: The commanded sample rate
 */
void HubControl::read_adj_samp_cmd(uint8_t *dest_node_id, uint8_t *samp_rate) {
    bool timeout = false;
    uint8_t memsize =
            sizeof(uint8_t) +
            sizeof(uint8_t);
    uint8_t *_buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    unsigned long start_time = millis();

    for (uint8_t i = 0; i < memsize; i++) {
        while (Serial.available() == 0 && millis() - start_time < UART_TIMEOUT)
            ; // Busy wait or timeout
        if (millis() - start_time >= UART_TIMEOUT)
            timeout = true;
        _buf[i] = Serial.read();
    }

    *dest_node_id = _buf[0];
    *samp_rate = _buf[1];

    free(_buf);

    if (timeout) {
        Serial.println(TIMEOUT_WARNING);
        delay(10);
    }
}

/**
 * @brief Send node data to the RasPi over UART
 * @param[in] origin_node_id: The node id the message is from
 * @param[in] send_time: The sender timestamp
 * @param[in] num_sensor_bytes: The number of sensor bytes
 * @param[in] data_buf: Pointer to the sensor data buffer
 */
void HubControl::send_node_data(uint8_t origin_node_id, time_t send_time, uint8_t num_sensor_bytes, uint8_t *data_buf) {
    uint8_t memsize =
            sizeof(origin_node_id) +
            sizeof(send_time) +
            sizeof(num_sensor_bytes) +
            (num_sensor_bytes * sizeof(uint8_t));
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    buf[0] = origin_node_id;
    buf[1] = time_to_uint8(send_time, 0);
    buf[2] = time_to_uint8(send_time, 1);
    buf[3] = time_to_uint8(send_time, 2);
    buf[4] = time_to_uint8(send_time, 3);
    buf[5] = num_sensor_bytes;

    for (uint8_t i = 6; i < memsize; i++) // Loop through starting from the sixth byte
        buf[i] = data_buf[i - 6];

    Serial.println(F("Preparing to send node data"));
    Serial.print(F("Time: "));
    Serial.println(send_time);
    delay(50);

    Serial.print(this->COMMAND_HEADER);
    Serial.write(HubCommand::StoreData);
    Serial.write((char *) buf, memsize);

    free(buf);
}

/**
 * @brief Notify the RasPi over UART that there is a new node on the network
 * @param[in] node_id: The new node's mesh id
 */
void HubControl::send_welcoming_node(uint8_t node_id) {
    uint8_t memsize =
            sizeof(node_id);
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));

    buf[0] = node_id;

    Serial.print(this->COMMAND_HEADER);
    Serial.write(HubCommand::SendWelcoming);
    Serial.write((char *) buf, memsize);
    Serial.println();

    free(buf);
}
