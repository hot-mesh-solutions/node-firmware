# Usage:
# * make clean
# * make all
# * make upload
#
# The following need to be defined:
# ARDUINO_MAKEFILE
# MONITOR_PORT

DEFAULT_NODE_SERIAL_NUMBER ?= 0x$(shell xxd -l 2 -p /dev/urandom | tr a-z A-Z)

BOARD_TAG = uno
BOARD_SUB = atmega328
BOARD_CLOCK = 16mhz
ARDUINO_LIBS = radiohead SPI SoftwareSerial EEPROM FastCRC MemoryFree adafruit_mcp3008 Time RTClib Wire
AVRDUDE_ARD_BAUDRATE = 115200
CXXFLAGS += -D__DEFAULT_SERIAL_NUMBER=$(DEFAULT_NODE_SERIAL_NUMBER)
CXXFLAGS += -D__FIRMWARE_VERSION=\"$(shell git symbolic-ref HEAD 2> /dev/null | cut -b 12-)-$(shell git describe --always --long --dirty --tags --abbrev=10)\"

ifdef HUB_FIRMWARE
CXXFLAGS += -D__HUB_FIRMWARE
endif

include $(ARDUINO_MAKEFILE)
