#include "ExternalRtc.h"

RTC_DS1307 rtc = RTC_DS1307();

/**
 * @brief Initialize this module
 * @return True if initialization was a success
 */
bool ExternalRtc::initialize() {
    pinMode(PIN_RTC_CS, OUTPUT);
    digitalWrite(PIN_RTC_CS, LOW); // Disable the chip
    // SPI RTC doesn't actually work, just fake it

    if (this->_initialized) // Don't double init
        return true;

    DEBUG_PRINT(F("Starting RTC initialization..."));

    if (! rtc.begin()) {
        DEBUG_PRINTLN(F("RTC begin failed"));
        return false;
    }

    if (! rtc.isrunning()) {
        DEBUG_PRINTLN(F("RTC not running"));
        this->set_time(-1); // Set time to 2099
    }

    if (! rtc.isrunning()) {
        DEBUG_PRINTLN(F("RTC not running"));
        return false; // Second time it is an issue
    }

    DEBUG_PRINTLN(INIT_MSG_SUCCESS);
    this->_initialized = true;
    return true;
}

/**
 * @brief Check if we have been initialized
 * @return True if we have been initialized
 */
bool ExternalRtc::is_initialized() {
    return this->_initialized;
}

/**
 * @brief Set the current time on the rtc
 * @param[in] time_to_set: Time to send to the rtc
 */
void ExternalRtc::set_time(time_t time_to_set) {
    rtc.adjust(DateTime((uint32_t) time_to_set)); // Set the time
}

/**
 * @brief Get the current time from the rtc
 * @return time_t from the rtc
 */
time_t ExternalRtc::get_time() {
    return rtc.now().unixtime();
}
