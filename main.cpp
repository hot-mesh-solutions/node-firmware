#include <Arduino.h>

#include "DebugSerial.h"
#include "DebugInterface.h"
#include "HardwareDefinition.h"
#include "StoredData.h"
#include "ExternalSensor.h"
#include "StatusLed.h"
#include "WdtSleep.h"
#include "WirelessRadio.h"
#include "ExternalAdc.h"
#include "ExternalRtc.h"
#include "HubControl.h"
#include "helpers.h"

/********************************************************************************************/
#define WAIT_FOR_USB_TIME (5000)
#define COMPUTER_BAUDRATE (115200)
#define MAX_FORWARING_LEN (5)
#define NULL_SERIAL_NUMBER (0xFFFF)

#define E_SENSOR_BAUDRATE (2400)
/********************************************************************************************/

enum State : uint8_t {
#ifdef __HUB_FIRMWARE
    Idle                   = 0x01,
    WelcomeNewNode         = 0x02,
    ReceiveSendData        = 0x03,
    SendNetworkCommand     = 0x04,
    SendAdjustTxPwr        = 0x05,
    SendAdjustSampRate     = 0x06
#else
    WaitForWelcome         = 0x01,
    Interrupted            = 0x02,
    TransmitData           = 0x03,
    ReceiveAdjustParameter = 0x04
#endif // __HUB_FIRMWARE
};

#ifdef __HUB_FIRMWARE
State curr_state = State::Idle;

PacketType last_type; // For storing what the type of the last packet is
#else
State curr_state = State::WaitForWelcome; // First thing we want to do is try to join the network

uint8_t hub_mesh_id = 0xFF; // This is populated with the hub id after receiving a HUB_WELCOME message
uint16_t remaining_time; // For storing how long is remaining after trying to sleep
PacketType last_type; // For storing what the type of the last packet is
#endif // __HUB_FIRMWARE

extern volatile bool _DEBUG_SERIAL_ESTABLISHED; // From DebugSerial.cpp

#ifdef __HUB_FIRMWARE

#define HUB_TX_PWR (10)
StoredData stored_data = StoredData();
WirelessRadio wireless_radio = WirelessRadio();
StatusLed status_led = StatusLed(PIN_STATUS_LED);
HubControl hub_control = HubControl();
#else
StoredData stored_data = StoredData();
WirelessRadio wireless_radio = WirelessRadio();
ExternalSensor external_sensor = ExternalSensor(E_SENSOR_BAUDRATE);
StatusLed status_led = StatusLed(PIN_STATUS_LED);
ExternalAdc external_adc = ExternalAdc();
ExternalRtc external_rtc = ExternalRtc();
DebugInterface debug_interface = DebugInterface(
        (uint8_t *)&curr_state,
        &stored_data,
        &external_sensor,
        &status_led,
        &wireless_radio,
        &external_adc,
        &external_rtc);
#endif // __HUB_FIRMWARE

void set_state(State new_state) {
    curr_state = new_state;
}

/**
 * @brief Initial code execution start
 */
void setup() {
    analogReference(EXTERNAL); // Set the AREF value to external

    status_led.initialize(); // Initialize the status LED controller first
    status_led.set_idle();

    Serial.begin(COMPUTER_BAUDRATE);
    Serial.println(F("Hi!"));

#ifdef __HUB_FIRMWARE
    _DEBUG_SERIAL_ESTABLISHED = true;
    Serial.println(F("Hot Mesh Solutions Hub Firmware"));
    Serial.print(F("Version: "));
    Serial.println(F(__FIRMWARE_VERSION));
    delay(200);

    // Main module initializations

    // Initialize stored data
    status_led.set_good();
    while (! stored_data.initialize()) {
        delay(1000);
        status_led.set_bad();
    }
    status_led.set_idle();
    delay(500);

    // Initialize Radio
    status_led.set_good();
    while (! wireless_radio.initialize(HUB_TX_PWR, DEFAULT_HUB_MESH_ID)) {
        delay(1000);
        status_led.set_bad();
    }
    status_led.set_idle();
    delay(500);

    Serial.println(F("Setup complete!"));
    status_led.set_good();
#else
    while(Serial.available() == 0 && millis() < WAIT_FOR_USB_TIME)
        ; // Wait until either a serial message is received or we have a timeout

    if (Serial.available() > 0) {
        _DEBUG_SERIAL_ESTABLISHED = true;
        DEBUG_PRINTLN(F("USB comms"));
    } else {
        Serial.println(F("Ech"));
        delay(10);
        _DEBUG_SERIAL_ESTABLISHED = false;
        Serial.end(); // We don't have computer serial coms
    }

    DEBUG_PRINTLN(F("Hot Mesh Solutions Node Firmware"));
    DEBUG_PRINT(F("Version: "));
    DEBUG_PRINTLN(F(__FIRMWARE_VERSION));
    delay(200);

    // Main module initializations

    // Initialize stored data
    status_led.set_good();
    while (! stored_data.initialize()) {
        delay(1000);
        status_led.set_bad();
        debug_interface.pause(3);
    }
    status_led.set_idle();
    delay(500);

    // Initialize Radio, depends on stored_data
    status_led.set_good();
    while (! wireless_radio.initialize(stored_data.settings.RFM95_tx_power, stored_data.settings.node_id)) {
        delay(1000);
        status_led.set_bad();
        debug_interface.pause(3);
    }
    status_led.set_idle();
    delay(500);

    // Initialize ADC
    status_led.set_good();
    while (! external_adc.initialize()) {
        delay(1000);
        status_led.set_bad();
        debug_interface.pause(3);
    }
    status_led.set_idle();
    delay(500);

    // Initialize RTC
    status_led.set_good();
    while (! external_rtc.initialize()) {
        delay(1000);
        status_led.set_bad();
        debug_interface.pause(3);
    }
    status_led.set_idle();
    delay(500);

    DEBUG_PRINTLN(F("Done setup!"));
    status_led.set_good();
    debug_interface.pause(3);
#endif // __HUB_FIRMWARE
}

/**
 * @brief Main state machine loop
 * \image latex Node_Firmware_Flowchart.png "Main node flowchart state machine overview" width=\textwidth
 * \image latex Hub_Firmware_Flowchart.png "Main hub flowchart state machine overview" width=\textwidth
 */
void loop() {
#ifdef __HUB_FIRMWARE

    switch (curr_state) {
        case State::Idle:
            Serial.println(F(">> Idle"));
            while (Serial.available() == 0 && ! wireless_radio.recv_mesh_or_timeout())
                ; // Busy wait

            if (Serial.available() != 0) {
                Serial.println(F("UART Int"));
                delay(2);

                set_state(State::SendNetworkCommand);
            } else {
                Serial.println(F("Radio Int"));
                delay(2);

                switch(last_type = wireless_radio.last_message_type()) {
                    case PacketType::SEND_DATA: set_state(State::ReceiveSendData); break;
                    case PacketType::NEW_NODE: set_state(State::WelcomeNewNode); break;

                    default:
                        Serial.print(F("Invalid packet type: "));
                        Serial.println((int)last_type);
                        break;
                }
            }

            break;

        case State::WelcomeNewNode:
            Serial.println(F(">> WelcomeNewNode"));

            {
                wireless_radio.unpack_NEW_NODE();

                uint8_t origin_node_id, mesh_dest, mesh_id;
                wireless_radio.get_last_mesh_metadata(&origin_node_id, &mesh_dest, &mesh_id);

                Serial.print(F("Welcoming "));
                Serial.print(origin_node_id);
                Serial.println(F(" to the network!"));
                delay(20);

                hub_control.send_welcoming_node(origin_node_id);

                time_t cur_time = hub_control.request_time();

                wireless_radio.send_HUB_WELCOME(origin_node_id, stored_data.settings.node_id, cur_time);
            }

            set_state(State::Idle);

            break;

        case State::ReceiveSendData:
            Serial.println(F(">> ReceiveSendData"));

            {
                time_t timestamp;
                uint8_t num_sensor_bytes;
                uint8_t *data_buf = (uint8_t *) calloc(16, sizeof(uint8_t));
                wireless_radio.unpack_SEND_DATA(&timestamp, &num_sensor_bytes, &data_buf);

                uint8_t origin_node_id, mesh_dest, mesh_id;
                wireless_radio.get_last_mesh_metadata(&origin_node_id, &mesh_dest, &mesh_id);

                hub_control.send_node_data(origin_node_id, timestamp, num_sensor_bytes, data_buf);

                free(data_buf);
            }

            set_state(State::Idle);

            break;

        case State::SendNetworkCommand:
            Serial.println(F(">> SendNetworkCommand"));

            {
                HubCommand command = hub_control.check_command_type();
                if (command == HubCommand::INVALID) { // Invalid command
                    set_state(State::Idle);
                    break;
                }

                switch (command) {
                    case HubCommand::SendAdjTXPwr:
                        set_state(State::SendAdjustTxPwr);
                        break;

                    case HubCommand::SendAdjSamp:
                        set_state(State::SendAdjustSampRate);
                        break;

                    default:
                        set_state(State::Idle);
                        break;
                }
            }

            break;

        case State::SendAdjustTxPwr:
            Serial.println(F(">> SendAdjustTxPwr"));

            {
                uint8_t dest_node_id;
                int8_t tx_pwr;

                hub_control.read_adj_tx_pwr_cmd(&dest_node_id, &tx_pwr);

                Serial.println(F("Sending ADJUST_TXPWR"));
                Serial.print(dest_node_id);
                Serial.print(F(" : "));
                Serial.println(tx_pwr);

                wireless_radio.send_ADJUST_TXPWR(dest_node_id, tx_pwr);
            }

            set_state(State::Idle);

            break;

        case State::SendAdjustSampRate:
            Serial.println(F(">> SendAdjustSampRate"));

            {
                uint8_t dest_node_id;
                uint8_t samp_rate;

                hub_control.read_adj_samp_cmd(&dest_node_id, &samp_rate);

                Serial.println(F("Sending ADJUST_SAMP"));
                Serial.print(dest_node_id);
                Serial.print(F(" : "));
                Serial.println(samp_rate);

                wireless_radio.send_ADJUST_SAMP(dest_node_id, samp_rate);
            }

            set_state(State::Idle);

            break;

        default:
            DEBUG_PRINT(F("State error! "));
            Serial.println((int)curr_state);
            break;
    }

#else

    switch (curr_state) {
        case State::WaitForWelcome:
            DEBUG_PRINTLN(F(">> WaitForWelcome"));

            wireless_radio.send_NEW_NODE(0xFF); // Broadcast to all

            remaining_time = wireless_radio.power_down_until_cad(15); // Sleep for 15s

            if (remaining_time == 0) {
                DEBUG_PRINTLN(F("Timeout"));
                set_state(State::TransmitData);
            } else {
                DEBUG_PRINT(F("CAD: "));
                DEBUG_PRINTLN(remaining_time);

                if (! wireless_radio.recv_mesh_after_cad()) {
                    break;
                }

                switch (last_type = wireless_radio.last_message_type()) {
                    case PacketType::HUB_WELCOME:
                        {
                            uint8_t hub_id;
                            time_t cur_time;
                            wireless_radio.unpack_HUB_WELCOME(&hub_id, &cur_time);

                            DEBUG_PRINT(F("Hub ID: "));
                            DEBUG_PRINTLN(hub_id);

                            DEBUG_PRINT(F("Time: "))
                            DEBUG_PRINTLN(cur_time);

                            hub_mesh_id = hub_id;
                            external_rtc.set_time(cur_time);
                            delay(10);

                            set_state(State::TransmitData); // Immediately TX data
                        }
                        break;

                    default:
                        DEBUG_PRINT(F("Invalid packet: "));
                        DEBUG_PRINTLN((int)last_type);
                        break;
                }
            }

            break;

        case State::Interrupted:
            DEBUG_PRINTLN(F(">> Interrupted"));

            if (remaining_time == 0) {
                DEBUG_PRINTLN(F("Timeout"));
                set_state(State::TransmitData);
            } else {
                DEBUG_PRINT(F("CAD: "));
                DEBUG_PRINTLN(remaining_time);

                wireless_radio.recv_mesh_after_cad();

                switch (last_type = wireless_radio.last_message_type()) {
                    case PacketType::ADJUST_SAMP:
                    case PacketType::ADJUST_TXPWR:
                        set_state(State::ReceiveAdjustParameter);
                        return; // Immediately go to state

                    default:
                        DEBUG_PRINT(F("Invalid packet: "));
                        DEBUG_PRINTLN((int)last_type);
                        break;
                }

                remaining_time = wireless_radio.power_down_until_cad(remaining_time); // Sleep for remaining time
                set_state(State::Interrupted);
            }

            break;

        case State::TransmitData:
            DEBUG_PRINTLN(F(">> TransmitData"));

            {
                time_t cur_time = 0;
                uint8_t num_sensor_data_bytes;
                uint8_t *sensor_data;

                DEBUG_PRINT(F("Retrieving battery: "));
                uint8_t batt_data = mapf(external_adc.read_battery(), 0, 5, 0x00, 0xFF); // Map 0->5V to 0->255
                DEBUG_PRINTLN(batt_data);

                DEBUG_PRINT(F("Retrieving solar: "));
                uint8_t solar_data = mapf(external_adc.read_solar(), 0, 12, 0x00, 0xFF); // Map 0->12V to 0->255
                DEBUG_PRINTLN(solar_data);

                DEBUG_PRINT(F("Retrieving temp: "));
                float temperature_data = external_sensor.check_sensor();
                DEBUG_PRINTLN(temperature_data)

                DEBUG_PRINT(F("Getting timestamp: "));
                cur_time = external_rtc.get_time();
                DEBUG_PRINTLN(cur_time)

                num_sensor_data_bytes = \
                        sizeof(SensorType::BATTERY_VOLTAGE) + sizeof(batt_data) +
                        sizeof(SensorType::SOLAR_VOLTAGE) + sizeof(solar_data) +
                        sizeof(SensorType::TEMPERATURE) + sizeof(temperature_data);
                sensor_data = (uint8_t *) calloc(1, num_sensor_data_bytes); // Allocate memory

                sensor_data[0] = SensorType::BATTERY_VOLTAGE;
                sensor_data[1] = batt_data;
                sensor_data[2] = SensorType::SOLAR_VOLTAGE;
                sensor_data[3] = solar_data;
                sensor_data[4] = SensorType::TEMPERATURE;
                sensor_data[5] = float_to_uint8(temperature_data, 0);
                sensor_data[6] = float_to_uint8(temperature_data, 1);
                sensor_data[7] = float_to_uint8(temperature_data, 2);
                sensor_data[8] = float_to_uint8(temperature_data, 3);

                wireless_radio.send_SEND_DATA(hub_mesh_id, cur_time, num_sensor_data_bytes, sensor_data);
                free(sensor_data);

                remaining_time = wireless_radio.power_down_until_cad(stored_data.settings.sleep_time); // Sleep for data rate
                set_state(State::Interrupted);
            }

            break;

        case State::ReceiveAdjustParameter:
            DEBUG_PRINTLN(F(">> ReceiveAdjustParameter"));

            switch (last_type) {
                case PacketType::ADJUST_SAMP:
                    DEBUG_PRINTLN(F("ADJUST_SAMP"));
                    {
                        uint8_t samp_rate;
                        wireless_radio.unpack_ADJUST_SAMP(&samp_rate);

                        if (samp_rate < 10) {
                            samp_rate = 10; // Hard limit max sample rate to 10
                        }

                        DEBUG_PRINT(F("samp_rate="));
                        DEBUG_PRINTLN(samp_rate);

                        stored_data.settings.sleep_time = samp_rate; // Update the sample rate
                        stored_data.update_settings(); // Store to EEPROM
                    }
                    break;

                case PacketType::ADJUST_TXPWR:
                    DEBUG_PRINTLN(F("ADJUST_TXPWR"));
                    {
                        int8_t tx_power;
                        wireless_radio.unpack_ADJUST_TXPWR(&tx_power);

                        DEBUG_PRINT(F("tx_power="));
                        DEBUG_PRINTLN(tx_power);

                        stored_data.settings.RFM95_tx_power = tx_power; // Update the TX power
                        stored_data.update_settings(); // Store to EEPROM
                        wireless_radio.set_tx_power(tx_power);
                    }
                    break;

                default:
                    DEBUG_PRINTLN(F("Invalid ADJUST: "));
                    DEBUG_PRINTLN((int)last_type);
                    break;
            }

            remaining_time = wireless_radio.power_down_until_cad(remaining_time); // Sleep for remaining time
            set_state(State::Interrupted);

            break;

        default:
            DEBUG_PRINT(F("State error! "));
            DEBUG_PRINTLN((int)curr_state);
            debug_interface.pause(3);
            break;
    }
#endif // __HUB_FIRMWARE
}
