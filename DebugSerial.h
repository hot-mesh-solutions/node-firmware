#ifndef NODE_FIRMWARE_DEBUGSERIAL_H
#define NODE_FIRMWARE_DEBUGSERIAL_H

#include <HardwareSerial.h>

#define DEBUG

extern volatile bool _DEBUG_SERIAL_ESTABLISHED; // From DebugSerial.cpp

#ifdef DEBUG
#define DEBUG_PRINT(...) if (_DEBUG_SERIAL_ESTABLISHED) { Serial.print( __VA_ARGS__ ); }
#define DEBUG_PRINTLN(...) if (_DEBUG_SERIAL_ESTABLISHED) { Serial.println( __VA_ARGS__ ); }
#else
#define DEBUG_PRINT(...) do{ } while ( false )
#define DEBUG_PRINTLN(...) do{ } while ( false )
#endif

#endif //NODE_FIRMWARE_DEBUGSERIAL_H
