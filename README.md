# Node/Hub Firmware
## Requirements
* You need to download/get [Arduino Makefile](https://github.com/sudar/Arduino-Makefile)
* When using the Makefile, you need to define
    * `ARDUINO_MAKEFILE`: The location of [Ardunio Makefile](https://github.com/sudar/Arduino-Makefile) (`/path/to/Arduino-Makefile/Arduino.mk`)
    * `MONITOR_PORT`: The port that the Arduino is connected to (`/dev/ttyUSB0`)
* Customized libraries
    * Clone into your Arduino library folder (`~/Arduino/libraries`?)
    * [RadioHead](https://gitlab.com/hot-mesh-solutions/libraries/radiohead)
    * [PCF2123](https://gitlab.com/hot-mesh-solutions/libraries/pcf2123)
    * [Adafruit_MCP3008](https://gitlab.com/hot-mesh-solutions/libraries/adafruit_mcp3008)
* [FastCRC](https://github.com/FrankBoesing/FastCRC)
* [MemoryFree](https://github.com/McNeight/MemoryFree)
* [Time](https://github.com/PaulStoffregen/Time)
* [RTClib](https://github.com/adafruit/RTClib)

You also may need to edit `/usr/avr/include/avr/pgmspace.h` and redefine the following macro:
```
#define PSTR(str) \
  (__extension__({ \
  PGM_P ptr;  \
  asm volatile \
  ( \
  ".pushsection .progmem.data, \"SM\", @progbits, 1" "\n\t" \
  "0: .string " #str                                 "\n\t" \
  ".popsection"                                      "\n\t" \
  ); \
  asm volatile \
  ( \
  "ldi %A0, lo8(0b)"                                 "\n\t" \
  "ldi %B0, hi8(0b)"                                 "\n\t" \
  : "=d" (ptr) \
  ); \
  ptr; \
  }))
```

## Building
`make clean ARDUINO_MAKEFILE=blah`

`make all ARDUINO_MAKEFILE=blah`
### Optional
* Define `DEFAULT_NODE_SERIAL_NUMBER` to set the node firmware's default SN, instead of picking a random hex value
    * Add `DEFAULT_NODE_SERIAL_NUMBER=BEEF` to the end of a make command
* Define `HUB_FIRMWARE` to build the hub firmware
    * Add `HUB_FIRWMARE=true` to the end of a make command

## Flashing
`make upload ARDUINO_MAKEFILE=blah MONITOR_PORT=/dev/blah`

## Building Documentation
* Install pdftex dependencies
    * `texlive-tocloft texlive-interfaces texlive-sectsty  texlive-multirow texlive-adjustbox texlive-tabu`
* Open `documentation/Doxyfile` in Doxywizard
* Click Run/Run Doxygen
* `cd documentation/output/latex`
* Edit `Makefile` to remove both instances of `makeindex refman.idx`
* `make -j4`
* Output is `refman.pdf`

