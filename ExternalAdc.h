#ifndef NODE_FIRMWARE_EXTERNALADC_H
#define NODE_FIRMWARE_EXTERNALADC_H

#include <Arduino.h>
#include <Adafruit_MCP3008.h>
#include "HardwareDefinition.h"
#include "DebugSerial.h"
#include "helpers.h"

#define USE_INTERNAL

#define INTERNAL_1V1_REFERENCE_CONSTANT (1125300L)

#define _5V_SUPPLY (5.0)
#define _3V3_SUPPLY (3.3)

#define MAX_ADC_RESOLUTION (1023)
#define MAX_ADC_REFERENCE (_3V3_SUPPLY)

#define MAX_SOLAR_VOLTAGE (5.6)

#define _3V3_DIVIDER (1.0)
#define _5V_DIVIDER (5.0 / (5.0 + 5.0))
#define SOLAR_DIVIDER (2.0 / (4.99 + 2.0))
#define BATT_DIVIDER (4.99 / (4.99 + 4.99))
#define MAIN_CM_MULTIPLIER (1.0)
#define EXT_CM_MULTIPLIER (1.0)

class ExternalAdc {
public:
    bool initialize();
    bool is_initialized();
    int read_channel(uint8_t);
    float read_solar();
    float read_battery();
    float read_main_current();
    float read_external_current();
    float read_5v_supply();
    float read_3v3_supply();
    long read_vref();

private:
    bool _initialized = false;
};

#endif //NODE_FIRMWARE_EXTERNALADC_H
