#ifndef NODE_FIRMWARE_STOREDDATA_H
#define NODE_FIRMWARE_STOREDDATA_H

#include <Arduino.h>
#include <EEPROM.h>
#include <FastCRC.h>
#include <Time.h>
#include "DebugSerial.h"
#include "helpers.h"

#define DEFAULT_HUB_MESH_ID (0x01)

struct SettingsStruct {
    uint16_t checksum;
    int8_t RFM95_tx_power;
    uint8_t sleep_time;
    uint16_t serial_number;
    uint8_t node_id;
};

struct SensordataStruct {
    time_t timestamp;
    uint8_t sensor_type;
    float sensor_data;
};

union SettingsToByteArr {
    SettingsStruct _struct;
    char _bytes[sizeof(SettingsStruct)];
};

static_assert(sizeof(SettingsStruct) <= 128, "SettingsStruct will not fit in 128 EEPROM");

#define DEFAULT_SETTINGS_START_ADDR (0)
#define DEFAULT_SETTINGS_END_ADDR (DEFAULT_SETTINGS_START_ADDR + sizeof(SettingsStruct) - 1)

#define ACTUAL_SETTINGS_START_ADDR (DEFAULT_SETTINGS_END_ADDR + 1)
#define ACTUAL_SETTINGS_END_ADDR (ACTUAL_SETTINGS_START_ADDR + sizeof(SettingsStruct) - 1)

#define SENSORDATA_START_ADDR (ACTUAL_SETTINGS_END_ADDR + 1)
#define SENSORDATA_END_ADDR (EEPROM.length())

class StoredData {
public:
    bool initialize();
    bool is_initialized();
    void print_settings();
    void fill_settings_from(uint16_t);
    uint16_t calculate_settings_checksum();
    uint16_t calculate_checksum(uint8_t *, uint16_t);
    void erase_all();
    void write_settings_to(uint16_t);
    void update_settings();
    void dump_eeprom(bool);
    void print_stored_sensor_data();
    void store_sensor_data(time_t, uint8_t, float);

    SettingsStruct settings = {
        .checksum = 0,
        .RFM95_tx_power = 10,
        .sleep_time = 30,
        .serial_number = __DEFAULT_SERIAL_NUMBER,
#ifdef __HUB_FIRMWARE
        .node_id = DEFAULT_HUB_MESH_ID
#else
        .node_id = (uint8_t)__DEFAULT_SERIAL_NUMBER
#endif // __HUB_FIRMWARE
    };

    uint16_t sensor_data_addr = SENSORDATA_START_ADDR;

private:
    bool _initialized = false;
};

#endif //NODE_FIRMWARE_STOREDDATA_H
