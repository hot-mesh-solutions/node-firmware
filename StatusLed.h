#ifndef NODE_FIRMWARE_STATUSLED_H
#define NODE_FIRMWARE_STATUSLED_H

#include <Arduino.h>
#include "HardwareDefinition.h"
#include "DebugSerial.h"
#include "helpers.h"

#define LED_PWM_STARTING (127)
#define LED_PWM_STARTUP_GOOD (255)
#define LED_PWM_STARTUP_BAD (30)
#define LED_PWM_IDLE (100)

class StatusLed {
public:
    StatusLed(uint8_t);
    bool initialize();
    bool is_initialized();
    void set_raw(uint8_t);
    void set_startup();
    void set_good();
    void set_bad();
    void set_idle();
    uint8_t get_pwm_value();

private:
    uint8_t pwm_value;
    uint8_t pin;
    bool _initialized = false;
};

#endif //NODE_FIRMWARE_STATUSLED_H
