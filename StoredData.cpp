#include "StoredData.h"

FastCRC16 CRC16;

/**
 * @brief Initialize this module
 * @return True if initialization was a success
 */
bool StoredData::initialize() {
    if (this->_initialized) // Don't double init
        return true;

    DEBUG_PRINT(F("Initializing stored settings..."));
    this->settings.checksum = this->calculate_settings_checksum();
    // On startup, settings struct is filled with default values
    // Write those values into the start of EEPROM
    // This is done after calculating the default checksum, so that the EEPROM doesn't need to be updated twice
    this->write_settings_to(DEFAULT_SETTINGS_START_ADDR);

    // Now we check that the stored settings are actually valid
    this->fill_settings_from(ACTUAL_SETTINGS_START_ADDR);
    // Settings struct has the actual EEPROM data in it now

    // Check that the CRC is correct
    if (this->settings.checksum != this->calculate_settings_checksum()) {
        DEBUG_PRINTLN(F("EEPROM memory has been corrupted! Writing default values"));
        this->fill_settings_from(DEFAULT_SETTINGS_START_ADDR);
        this->write_settings_to(ACTUAL_SETTINGS_START_ADDR);
        DEBUG_PRINTLN(F("Done"));
    }

    DEBUG_PRINTLN(INIT_MSG_SUCCESS);

    this->print_settings();

    this->_initialized = true;
    return true;
}

/**
 * @brief Check if we have been initialized
 * @return True if we have been initialized
 */
bool StoredData::is_initialized() {
    return this->_initialized;
}

/**
 * @brief Print to the Serial line the stored settings
 */
void StoredData::print_settings() {
    DEBUG_PRINT(F("checksum\t")); DEBUG_PRINTLN(this->settings.checksum);
    DEBUG_PRINT(F("RFM95_tx_power\t")); DEBUG_PRINTLN(this->settings.RFM95_tx_power);
    DEBUG_PRINT(F("sleep_time\t")); DEBUG_PRINTLN(this->settings.sleep_time);
    DEBUG_PRINT(F("serial_number\t")); DEBUG_PRINTLN(this->settings.serial_number, HEX);
    DEBUG_PRINT(F("node_id\t")); DEBUG_PRINTLN(this->settings.node_id, HEX);
}

/**
 * @brief Fill the settings struct from an address in EEPROM
 * @param[in] addr: The address in EEPROM to start filling from
 */
void StoredData::fill_settings_from(uint16_t addr) {
    // Populate the settings struct from EEPROM memory
    EEPROM.get(addr, this->settings);
}

/**
 * @brief Calculate the checksum of the current settings struct
 * @return uint16_t checksum of the settings struct
 */
uint16_t StoredData::calculate_settings_checksum() {
    uint8_t *crc_ptr;
    uint16_t crc_size;
    // Get a reference to the start of the settings struct, except for the checksum part
    crc_ptr = (uint8_t *)&this->settings + sizeof(this->settings.checksum);
    crc_size = sizeof(SettingsStruct) - sizeof(this->settings.checksum);

    return this->calculate_checksum(crc_ptr, crc_size); // Return the checksum
}

/**
 * @brief Calculate a checksum starting from `*ptr` of size `size`
 * @param[in] ptr: Pointer to start from
 * @param[in] size: Length of data to check
 * @return uint16_t checksum of the data
 */
uint16_t StoredData::calculate_checksum(uint8_t *ptr, uint16_t size) {
    return CRC16.ccitt(ptr, size);
}

/**
 * @brief Fill the entire EEPROM with 0
 */
void StoredData::erase_all() {
    DEBUG_PRINTLN(F("Erasing all of EEPROM"));
    for (unsigned int i = 0; i < EEPROM.length(); i++) {
        EEPROM.update(i, 0);
    }
    DEBUG_PRINTLN(F("Erasing done"));
}

/**
 * @brief Write the current settings struct to an address in EEPROM
 * @param[in] addr: The address in EEPROM to write the settings to
 */
void StoredData::write_settings_to(uint16_t addr) {
    // Write the entire SettingsStruct into EEPROM starting at addr
    EEPROM.put(addr, this->settings);
}

void StoredData::update_settings() {
    this->settings.checksum = this->calculate_settings_checksum();
    this->write_settings_to(ACTUAL_SETTINGS_START_ADDR);
}

/**
 * @brief Print over Serial the entire EEPROM memory, hex encoded
 */
void StoredData::dump_eeprom(bool raw_output = false) {
    uint8_t value;

    for (unsigned int i = 0; i < EEPROM.length(); i += 1) {
        if (!raw_output) {
            if (i % 16 == 0) {
                DEBUG_PRINTLN();
                DEBUG_PRINT(i, HEX);
                DEBUG_PRINT(F(":\t"));
            } else if (i % 8 == 0) {
                DEBUG_PRINT(F(" "));
            }
        }

        EEPROM.get(i, value);
        if (value <= 0xF) {
            DEBUG_PRINT(F("0")); // Print a leading zero
        }
        DEBUG_PRINT(value, HEX);
    }
    DEBUG_PRINTLN();
}

/**
 * @brief Store sensor data in EEPROM
 * @param[in] sensor_time: Timestamp of the sensor data
 * @param[in] sensor_data: Sensor data
 */
void StoredData::store_sensor_data(time_t sensor_time, uint8_t sensor_type, float sensor_data) {
    SensordataStruct s = {
        .timestamp = sensor_time,
        .sensor_type = sensor_type,
        .sensor_data = sensor_data
    };

    EEPROM.put(this->sensor_data_addr, s);
    this->sensor_data_addr += sizeof(SensordataStruct);

    // Check if we're going to overflow
    if (this->sensor_data_addr + sizeof(SensordataStruct) >= EEPROM.length()) {
        DEBUG_PRINTLN(F("EEPROM sensor overflow, resetting"));
        this->sensor_data_addr = SENSORDATA_START_ADDR;
    }
}

void StoredData::print_stored_sensor_data() {

}
