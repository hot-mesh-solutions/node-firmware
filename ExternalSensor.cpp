#include "ExternalSensor.h"

extern volatile bool _DEBUG_SERIAL_ESTABLISHED; // From DebugSerial.cpp

/**
 * @brief Constructor for the ExternalSensor class
 * @param[in] baudrate: Baudrate for the external sensor
 */
ExternalSensor::ExternalSensor(long baudrate) {
    pinMode(PIN_E_SENSOR_PWR, OUTPUT);
    this->baudrate = baudrate;
    this->set_power(false);
}

/**
 * @brief Turn on or off power to the external sensor
 * @param[in] powerOn: True to turn power on
 */
void ExternalSensor::set_power(bool powerOn) {
    if (powerOn) {
        digitalWrite(PIN_E_SENSOR_PWR, HIGH);
        this->_is_powered = true;
    } else {
        digitalWrite(PIN_E_SENSOR_PWR, LOW);
        this->_is_powered = false;
    }
}

/**
 * @brief Check if the external sensor is currently being powered
 * @return True if the sensor is being powered
 */
bool ExternalSensor::is_powered() {
    return this->_is_powered;
}

/**
 * @brief Read the sensor value
 * @return Value of the sensor
 */
float ExternalSensor::check_sensor() {
    if (_DEBUG_SERIAL_ESTABLISHED) {
        DEBUG_PRINTLN(F("Faking sensor data"));
        delay(500);
        return -1.1802469E29; // 0xDEADBEEF
    }

    union {
        float _union_float;
        uint8_t _union_buf[sizeof(float)];
    };

    flush_serial_buffer();
    Serial.end();
    Serial.begin(this->baudrate);
    while (!Serial)
        ; // Wait for serial

    this->set_power(true);

    uint8_t i;
    for (i = 0; !Serial.available() && i < SENSOR_WAIT_ATTEMPTS; i++) {
        DEBUG_PRINT(F("."));
        delay(SENSOR_WAIT_DELAY);
    }

    if (i == SENSOR_WAIT_ATTEMPTS) {
        return NAN; // Failed to read from sensor
    }

    // Loop through, reading the serial com.
    // Exit when we are about to overrun our storage buffer
    for (uint8_t i = 0; i < sizeof(_union_buf); i++) {
        _union_buf[i] = Serial.read();
    }

    Serial.end();
    this->set_power(false);

    return _union_float;
}
