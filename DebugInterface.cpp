#include "DebugInterface.h"

extern volatile bool _DEBUG_SERIAL_ESTABLISHED; // From DebugSerial.cpp

/**
 * @brief Constructor for the DebugInterface class
 * @param[in] stored_data: Pointer to the initialized StoredData class
 * @param[in] external_sensor: Pointer to the initialized ExternalSensor class
 * @param[in] status_led: Pointer to the initialized StatusLed class
 * @param[in] wireless_radio: Pointer to the initialized WirelessRadio class
 * @param[in] external_adc: Pointer to the initialized ExternalAdc class
 * @param[in] external_rtc: Pointer to the initialized ExternalRtc class
 */
DebugInterface::DebugInterface(
        uint8_t *current_state,
        StoredData *stored_data,
        ExternalSensor *external_sensor,
        StatusLed *status_led,
        WirelessRadio *wireless_radio,
        ExternalAdc *external_adc,
        ExternalRtc *external_rtc) {

#ifdef BUILD_DEBUG_INTERFACE
    this->cur_state = current_state;
    this->stored_data = stored_data;
    this->external_sensor = external_sensor;
    this->status_led = status_led;
    this->wireless_radio = wireless_radio;
    this->external_adc = external_adc;
    this->external_rtc = external_rtc;
#endif // BUILD_DEBUG_INTERFACE
}

/**
 * @brief Pause and see if the user wants to start the debug menu
 * @param[in] countdown_time: Time to wait until the program just continues
 * @return True if the user has initiated the debug menu
 */
bool DebugInterface::pause(uint8_t countdown_time) {
#ifdef BUILD_DEBUG_INTERFACE
    if (!_DEBUG_SERIAL_ESTABLISHED) // Don't do anything if we don't have serial coms
        return false;

    DEBUG_PRINTLN(PAUSE_TEXT);
    delay(1000);
    flush_serial_buffer();

    uint8_t countdown = countdown_time;
    while(Serial.available() == 0 && countdown != 0) {
        DEBUG_PRINT(countdown); DEBUG_PRINT(F("..."));
        countdown--;
        delay(1000);
    }
    DEBUG_PRINT("\r\n");

    if (Serial.available() == 0 || Serial.read() == 'c') {
        flush_serial_buffer();
        return false;
    }

    flush_serial_buffer();
    this->main_menu();
    return true;
#else
    return false;
#endif // BUILD_DEBUG_INTERFACE
}

#ifdef BUILD_DEBUG_INTERFACE

/**
 * @brief Display the debug interface main menu
 */
void DebugInterface::main_menu() {
    bool in_menu = true;

    while (in_menu) {
        DEBUG_PRINT(MAIN_MENU_TEXT);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }

        switch (Serial.read()) {
            case 'g': this->general_status_menu(); break;
            case 's': this->settings_menu(); break;
            case 'x': this->external_sensor_menu(); break;
            case 'r': this->radio_menu(); break;
            case 'a': this->adc_menu(); break;
            case 't': this->rtc_menu(); break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Display the debug interface settings menu
 */
void DebugInterface::settings_menu() {
    bool in_menu = true;

    while (in_menu) {
        DEBUG_PRINT(SETTINGS_MENU_TEXT);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }

        switch (Serial.read()) {
            case 'i': this->stored_data->initialize(); break;
            case 'p': this->stored_data->print_settings(); break;
            case 'd': this->stored_data->dump_eeprom(false); break;
            case 'D': this->stored_data->dump_eeprom(true); break;
            case 's': this->set_serial_number(); break;
            case '!': this->stored_data->erase_all(); break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Display the debug interface adc menu
 */
void DebugInterface::adc_menu() {
    bool in_menu = true;

    while (in_menu) {
        DEBUG_PRINT(SPI_ADC_MENU_SCREEN);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }

        switch (Serial.read()) {
            case 'i': this->external_adc->initialize(); break;
            case 'r':
                this->external_rtc->get_time();
                for(int i = 0; i < 8; i++) {
                    DEBUG_PRINT(i);
                    DEBUG_PRINT(F(":\t"));
                    DEBUG_PRINTLN(this->external_adc->read_channel(i));
                }

                DEBUG_PRINT(F("5V:\t")); DEBUG_PRINTLN(this->external_adc->read_5v_supply());
                DEBUG_PRINT(F("3V3:\t")); DEBUG_PRINTLN(this->external_adc->read_3v3_supply());
                DEBUG_PRINT(F("Solar:\t")); DEBUG_PRINTLN(this->external_adc->read_solar());
                DEBUG_PRINT(F("Batt:\t")); DEBUG_PRINTLN(this->external_adc->read_battery());
                DEBUG_PRINT(F("Main Current:\t")); DEBUG_PRINTLN(this->external_adc->read_main_current());
                DEBUG_PRINT(F("Ext Current:\t")); DEBUG_PRINTLN(this->external_adc->read_external_current());

                break;
            case 'v': DEBUG_PRINTLN(this->external_adc->read_vref()); break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Display the debug interface rtc menu
 */
void DebugInterface::rtc_menu() {
    bool in_menu = true;

    while (in_menu) {
        DEBUG_PRINT(SPI_RTC_MENU_SCREEN);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }

        switch (Serial.read()) {
            case 'i': this->external_rtc->initialize(); break;
            case 'g': DEBUG_PRINTLN(this->external_rtc->get_time()); break;
            case 'S': this->external_rtc->set_time((time_t) DateTime(F(__DATE__), F(__TIME__)).unixtime()); break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Display the debug interface external sensor menu
 */
void DebugInterface::external_sensor_menu() {
    bool in_menu = true;

    while (in_menu) {
        DEBUG_PRINT(EXTERNAL_SENSOR_MENU_SCREEN);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }

        switch (Serial.read()) {
            case 't':
                if (this->external_sensor->is_powered()) {
                    this->external_sensor->set_power(false);
                    DEBUG_PRINTLN(F("OFF"));
                } else {
                    this->external_sensor->set_power(true);
                    DEBUG_PRINTLN(F("ON"));
                }
                break;
            case 'c':
                {
                    float sensor_val = this->external_sensor->check_sensor();
                    DEBUG_PRINTLN(sensor_val);
                }
                break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Display the debug interface radio menu
 */
void DebugInterface::radio_menu() {
    bool in_menu = true;
    uint16_t remaining_time;

    while (in_menu) {
        DEBUG_PRINT(RADIO_MENU_SCREEN);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }

        switch (Serial.read()) {
            case '0': this->wireless_radio->set_tx_power(-50); break;
            case '1': this->wireless_radio->set_tx_power(3); break;
            case '2': this->wireless_radio->set_tx_power(7); break;
            case '3': this->wireless_radio->set_tx_power(10); break;
            case '4': this->wireless_radio->set_tx_power(50); break;
            case 'i': this->wireless_radio->initialize(this->stored_data->settings.RFM95_tx_power, 0x02); break;
            case 's': this->send_wireless_message(); break;
            case 't': this->wireless_radio->send_test(); break;
            case 'r':
                DEBUG_PRINTLN(F("30s timeout"));
                delay(5);

                remaining_time = wireless_radio->power_down_until_cad(30);

                if (remaining_time == 0) {
                    DEBUG_PRINTLN(F("Timeout"));
                } else {
                    DEBUG_PRINT(F("Remaining time: "));
                    DEBUG_PRINTLN(remaining_time);

                    wireless_radio->recv_mesh_after_cad();
                }

                break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Display the debug interface general status menu
 */
void DebugInterface::general_status_menu() {
    bool in_menu = true, auto_update = false;

    while (in_menu) {
        DEBUG_PRINT(GENERAL_STATUS_MENU_SCREEN);
        DEBUG_PRINTLN(EXIT_OPTION_TEXT);
        this->print_status();
        DEBUG_PRINTLN(PROMPT_TEXT);

        while (Serial.available() == 0 && !auto_update) {
            delay(MENU_WAIT_AVAILABLE_TIME);
        }
        if (auto_update) {
            delay(AUTO_UPDATE_DELAY); // Still wait a tad
        }

        switch (Serial.read()) {
            case 'a': auto_update = ! auto_update; break;
            case '1': this->status_led->set_raw(0); break;
            case '2': this->status_led->set_raw(64); break;
            case '3': this->status_led->set_raw(128); break;
            case '4': this->status_led->set_raw(191); break;
            case '5': this->status_led->set_raw(255); break;
            case 's': this->set_state(); break;
            case 'e': DEBUG_PRINTLN(EXIT_MSG); in_menu = false; break;
            default: if (!auto_update) DEBUG_PRINTLN(INVALID_MSG); break;
        }
    }
}

/**
 * @brief Print a general status of what is going on with the node
 */
void DebugInterface::print_status() {
    DEBUG_PRINT(F("RAM: "));
    DEBUG_PRINTLN(freeMemory());
    char buf[256];
    char solar_str[6] = {0}, batt_str[6] = {0}, main_c_str[6] = {0}, ext_c_str[6] = {0};

    dtostrf(this->external_adc->read_solar(), 4, 2, solar_str);
    dtostrf(this->external_adc->read_battery(), 4, 2, batt_str);
    dtostrf(this->external_adc->read_main_current(), 4, 2, main_c_str);
    dtostrf(this->external_adc->read_external_current(), 4, 2, ext_c_str);

    time_t cur_time = this->external_rtc->get_time();

    sprintf_P(buf, GENERAL_STATUS_TEXT,
            this->wireless_radio->is_initialized(), this->stored_data->is_initialized(), this->external_adc->is_initialized(), this->external_rtc->is_initialized(),
            this->stored_data->settings.serial_number, this->stored_data->settings.node_id, *(this->cur_state),
            asctime(gmtime(&cur_time)), (unsigned long) cur_time,
            this->status_led->get_pwm_value(),
            solar_str, batt_str, main_c_str, ext_c_str);
    DEBUG_PRINT(buf);
}

/**
 * @brief Send a wireless message, which is generated interactively
 */
void DebugInterface::send_wireless_message() {
    uint8_t in_char, datasize, memsize, *buf = (uint8_t *) calloc(memsize = RH_RF95_MAX_MESSAGE_LEN, sizeof(uint8_t)); // 16 Byte buffer

    DEBUG_PRINT(F("> "));
    datasize = 0;
    for (uint8_t i = 0; i < memsize - 1; datasize++, i++) {
        while (Serial.available() == 0)
            ; // Block
        in_char = Serial.read();
        if (in_char == 0xD)
            break; // Exit when enter is pressed
        buf[i] = in_char;
        Serial.write(in_char);
    }
    datasize += 1; // Add space for null-terminated string
    DEBUG_PRINTLN();
    DEBUG_PRINT("Sending: ");
    DEBUG_PRINTLN((char *) buf);
    this->wireless_radio->send_mesh_data(buf, datasize, 0xFF); // Multicast
    free(buf);
}

void DebugInterface::set_serial_number() {
    uint8_t in_char, hex_value;
    uint16_t new_sn = 0;

    DEBUG_PRINT(F("SN> "));

    for (uint8_t i = 0; i < 4; i++) {
        while (Serial.available() == 0)
            ; // Block
        in_char = Serial.read();
        if (in_char == 0xD)
            break; // Exit when enter is pressed

        Serial.write(in_char);

        hex_value = in_char - '0'; // Convert from ascii to hex

        switch (i) {
            case 0: new_sn |= hex_value << 12; break;
            case 1: new_sn |= hex_value << 8; break;
            case 2: new_sn |= hex_value << 4; break;
            case 3: new_sn |= hex_value << 0; break;
            default:
                break;
        }
    }
    Serial.println();

    this->stored_data->settings.serial_number = new_sn;
    this->stored_data->update_settings();
}

void DebugInterface::set_state() {
    uint8_t in_char, hex_value, new_state = 0;

    DEBUG_PRINT(F("State> "));

    for (uint8_t i = 0; i < 2; i++) {
        while (Serial.available() == 0)
            ; // Block
        in_char = Serial.read();
        if (in_char == 0xD)
            break; // Exit when enter is pressed

        Serial.write(in_char);

        hex_value = in_char - '0'; // Convert from ascii to hex

        switch (i) {
            case 0: new_state |= hex_value << 4; break;
            case 1: new_state |= hex_value << 0; break;
            default:
                break;
        }
    }
    Serial.println();
    *(this->cur_state) = new_state;
}
#endif // BUILD_DEBUG_INTERFACE
