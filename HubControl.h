#ifndef NODE_FIRMWARE_HUBCONTROL_H
#define NODE_FIRMWARE_HUBCONTROL_H

#include <Arduino.h>
#include <Time.h>
#include "helpers.h"

#define UART_TIMEOUT (5000)
#define TIMEOUT_WARNING (F("Warning: timeout"))

enum HubCommand : uint8_t {
    INVALID          = 0x0,
    RequestTime      = 'T', // Outgoing
    /**
     * @brief Send a StoreData
     * Number of bytes: 2 + 4 + 1 + (1 * num_data_bytes)
     * Format:
     * uint16_t: Sender SN
     * time_t: Sender timestamp
     * uint8_t: Number of data bytes
     * uint8_t... : Sensor data:
     *      uint8_t: Sensor type (ExternalSensor.h)
     *      data_type: SensorType::BATTERY_VOLTAGE -> uint8_t
     *                 SensorType::SOLAR_VOLTAGE -> uint8_t
     *                 SensorType::TEMPERATURE -> float
     */
    StoreData        = 'D', // Outgoing
    SendAdjTXPwr     = 'P', // Incoming
    SendAdjSamp      = 'S', // Incoming
    SendWelcoming    = 'W'
};

class HubControl {
public:
    time_t request_time();
    HubCommand check_command_type();
    void read_adj_tx_pwr_cmd(uint8_t *, int8_t *);
    void read_adj_samp_cmd(uint8_t *, uint8_t *);
    void send_node_data(uint8_t, time_t, uint8_t, uint8_t *);
    void send_welcoming_node(uint8_t);

private:
    bool valid_hub_command(char);

    static const char COMMAND_HEADER[4];
};

#endif //NODE_FIRMWARE_HUBCONTROL_H
