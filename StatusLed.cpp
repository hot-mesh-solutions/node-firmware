#include "StatusLed.h"

/**
 * @brief Constructor for the StatusLed class
 * @param[in] pin: The status LED pin
 */
StatusLed::StatusLed(uint8_t pin) {
    this->pwm_value = 0;
    this->pin = pin;
}

/**
 * @brief Initialize this module
 * @return True if initialization was a success
 */
bool StatusLed::initialize() {
    if (this->_initialized) // Don't double init
        return true;

    pinMode(PIN_STATUS_LED, OUTPUT); // Set the status LED to an output
    this->pwm_value = LED_PWM_STARTING;
    analogWrite(PIN_STATUS_LED, LED_PWM_STARTING); // Set the value to starting

    DEBUG_PRINTLN(INIT_MSG_SUCCESS);
    this->_initialized = true;
    return true;
}

/**
 * @brief Check if we have been initialized
 * @return True if we have been initialized
 */
bool StatusLed::is_initialized() {
    return this->_initialized;
}

/**
 * @brief Set a raw LED PWM value
 * @param[in] pwm_value: PWM value to set the LED to
 */
void StatusLed::set_raw(uint8_t pwm_value) {
    this->pwm_value = pwm_value;
    analogWrite(PIN_STATUS_LED, pwm_value);
}

/**
 * @brief Set the LED to the startup state
 */
void StatusLed::set_startup() {
    this->pwm_value = LED_PWM_STARTING;
    analogWrite(PIN_STATUS_LED, LED_PWM_STARTING);
}

/**
 * @brief Set the LED to the good state
 */
void StatusLed::set_good() {
    this->pwm_value = LED_PWM_STARTUP_GOOD;
    analogWrite(PIN_STATUS_LED, LED_PWM_STARTUP_GOOD);
}

/**
 * @brief Set the LED to the bad state
 */
void StatusLed::set_bad() {
    this->pwm_value = LED_PWM_STARTUP_BAD;
    analogWrite(PIN_STATUS_LED, LED_PWM_STARTUP_BAD);
}

/**
 * @brief Set the LED to the idle state
 */
void StatusLed::set_idle() {
    this->pwm_value = LED_PWM_IDLE;
    analogWrite(PIN_STATUS_LED, LED_PWM_IDLE);
}

/**
 * @brief Get the current PWM value of the LED
 * @return uint8_t of the LED PWM value
 */
uint8_t StatusLed::get_pwm_value() {
    return this->pwm_value;
}