#ifndef NODE_FIRMWARE_EXTERNALRTC_H
#define NODE_FIRMWARE_EXTERNALRTC_H

#include <Arduino.h>
#include <Time.h>
#include "RTClib.h"
#include "helpers.h"
#include "DebugSerial.h"
#include "HardwareDefinition.h"

#define INITIAL_RTC_TIME (0)

class ExternalRtc {
public:
    bool initialize();
    bool is_initialized();
    void set_time(time_t);
    time_t get_time();

private:
    bool _initialized = false;
    tmElements_t scratch_tm;
};

#endif //NODE_FIRMWARE_EXTERNALRTC_H
