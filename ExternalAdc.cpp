#include "ExternalAdc.h"

Adafruit_MCP3008 adc = Adafruit_MCP3008();

/**
 * @brief Initialize this module
 * @return True if initialization was a success
 */
bool ExternalAdc::initialize() {
    if (this->_initialized) // Don't double init
        return true;

    DEBUG_PRINT(F("Starting ADC initialization..."));

#ifndef USE_INTERNAL
    if (! adc.begin(PIN_ADC_CS, SPI_HW_SPEED)) {
        DEBUG_PRINTLN(INIT_MSG_FAILED);
        return false;
    }

    delay(10); // Wait a lil
#else
    pinMode(PIN_ADC_CS, OUTPUT);
    digitalWrite(PIN_ADC_CS, LOW); // Disable the chip
#endif

    bool everything_is_zero = true;
    uint16_t channel_value;
    for (int i = 0; i < 8; i++) {
        channel_value = this->read_channel(i);
        if (channel_value != 0) {
            DEBUG_PRINT(i);
            DEBUG_PRINT(F(" is not 0: "));
            DEBUG_PRINTLN(channel_value);
            everything_is_zero = false;
        }
    }

    if (everything_is_zero) {
        DEBUG_PRINTLN(INIT_MSG_FAILED);
        return false;
    }

    DEBUG_PRINTLN(INIT_MSG_SUCCESS);
    this->_initialized = true;
    return true;
}

/**
 * @brief Check if we have been initialized
 * @return True if we have been initialized
 */
bool ExternalAdc::is_initialized() {
    return this->_initialized;
}

/**
 * @brief Read a raw ADC channel
 * @param[in] channel: The ADC channel to read from
 * @return The read channel, representated as an integer
 */
int ExternalAdc::read_channel(uint8_t channel) {
#ifdef USE_INTERNAL
    if (channel > 5) {
        return 0; // We only have 5 internal channels
    }
    switch (channel) {
        case 0: return analogRead(A0);
        case 1: return analogRead(A3);
        case 2: return analogRead(A1);
        case 3: return analogRead(A2);
        default:
            DEBUG_PRINT(F("Invalid ADC read: "));
            DEBUG_PRINTLN(channel);
            return -1;
    }
#else
    return adc.readADC(channel);
#endif
}

/**
 * @brief Read the 5V supply rail
 * @return 5V rail voltage
 */
float ExternalAdc::read_5v_supply() {
//    return ((analogRead(A5) * MAX_ADC_REFERENCE) / MAX_ADC_RESOLUTION) / _3V3_DIVIDER;
    return 0; // Disabled because of I2C RTC
}

/**
 * @brief Read the 3v3 supply rail
 * @return 3V3 rail voltage
 */
float ExternalAdc::read_3v3_supply() {
//    return ((analogRead(A4) * MAX_ADC_REFERENCE) / MAX_ADC_RESOLUTION) / _3V3_DIVIDER;
    return 0; // Disabled because of I2C RTC
}

/**
 * @brief Read the solar panel voltage
 * @return Voltage of the solar panel
 */
float ExternalAdc::read_solar() {
    return ((this->read_channel(0) * MAX_ADC_REFERENCE) / MAX_ADC_RESOLUTION) / SOLAR_DIVIDER;
}

/**
 * @brief Read the battery voltage
 * @return Voltage of the battery
 */
float ExternalAdc::read_battery() {
    return ((this->read_channel(1) * MAX_ADC_REFERENCE) / MAX_ADC_RESOLUTION) / BATT_DIVIDER;
}

/**
 * @brief Read the current draw from the battery
 * @return Current that is coming from the battery
 */
float ExternalAdc::read_main_current() {
    return this->read_channel(2) * MAIN_CM_MULTIPLIER;
}

/**
 * @brief Read the current draw from the external sensor
 * @return Current that is going to the external sensor
 */
float ExternalAdc::read_external_current() {
    return this->read_channel(3) * MAIN_CM_MULTIPLIER;
}

/**
 * @brief Read the ATmega reference voltage
 * @return The reference voltage, in mV
 */
long ExternalAdc::read_vref() {
    long result;
    // Read 1.1V reference against AVcc
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
    delay(2); // Wait for Vref to settle
    ADCSRA |= _BV(ADSC); // Convert
    while (bit_is_set(ADCSRA,ADSC))
        ;
    result = ADCL;
    result |= ADCH << 8;
    result = INTERNAL_1V1_REFERENCE_CONSTANT / result; // Back-calculate AVcc in mV
    return result;
}
