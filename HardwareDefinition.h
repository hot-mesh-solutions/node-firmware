#ifndef NODE_FIRMWARE_HARDWAREDEFINITION_H
#define NODE_FIRMWARE_HARDWAREDEFINITION_H

#define SPI_HW_SPEED (1000000)  // 1MHz

#define ATMEGA328P_PU_HARDWARE
//#define NANO_CONVERSION_HARDWARE


#ifdef ATMEGA328P_PU_HARDWARE
#define PIN_5V_DIVIDER (A5)
#define PIN_3V3_DIVIDER (A4)
#define PIN_BACKUP_ADC1 (A3)
#define PIN_BACKUP_ADC3 (A2)
#define PIN_BACKUP_ADC2 (A1)
#define PIN_BACKUP_ADC0 (A0)

#define PIN_BATT_PGOOD (10)
#define PIN_STATUS_LED (9)

#define PIN_RTC_INT (3)
#define PIN_RTC_CS (6)

#define PIN_ADC_CS (7)

#define PIN_E_SENSOR_PWR (4)

#define PIN_RFM95_DIO0 (2) // RFM95 Interrupt pin
#define PIN_RFM95_RESET (8) // RFM95 Reset pin
#define PIN_RFM95_CS (5) // RFM95 Chip Select
#endif

#ifdef NANO_CONVERSION_HARDWARE
#define PIN_5V_DIVIDER (A7)
#define PIN_3V3_DIVIDER (A6)
#define PIN_BACKUP_ADC1 (A5)
#define PIN_BACKUP_ADC3 (A4)
#define PIN_BACKUP_ADC2 (A3)
#define PIN_BACKUP_ADC0 (A2)

#define PIN_BATT_PGOOD (A0)
#define PIN_STATUS_LED (6)

#define PIN_RTC_INT (3)
#define PIN_RTC_CS (8)

#define PIN_ADC_CS (9)

#define PIN_E_SENSOR_PWR (4)

#define PIN_RFM95_DIO0 (2) // RFM95 Interrupt pin
#define PIN_RFM95_RESET (10) // RFM95 Reset pin
#define PIN_RFM95_CS (7) // RFM95 Chip Select
#endif

#endif //NODE_FIRMWARE_HARDWAREDEFINITION_H
