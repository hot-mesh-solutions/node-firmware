#include "WirelessRadio.h"

RH_RF95 rf95_radio = RH_RF95(PIN_RFM95_CS, PIN_RFM95_DIO0);

/**
 * @brief Initialize this module
 * @return True if initialization was a success
 */
bool WirelessRadio::initialize(uint8_t tx_power, uint8_t mesh_address) {
    if (this->_initialized) // Don't double init
        return true;

    DEBUG_PRINT(F("Starting radio initialization..."));

    this->mesh_manager = new RHMesh(rf95_radio, mesh_address);

    DEBUG_PRINT(F("Mesh manager "));
    if (! this->mesh_manager->init()) {
        DEBUG_PRINTLN(INIT_MSG_FAILED);
        return false;
    }

    DEBUG_PRINT(F("Radio init "));
    if (!this->rfm95_easy_init(RFM95_TX_TYPE, RFM95_FREQ, tx_power)) {
        DEBUG_PRINTLN(INIT_MSG_FAILED);
        return false;
    }

    DEBUG_PRINT(F("TX test "));
    this->set_tx_power(-50); // Set to low power for test
    if (! this->send_test()) {
        DEBUG_PRINTLN(INIT_MSG_FAILED);
        return false;
    }
    this->set_tx_power(tx_power); // Go back to normal power

    DEBUG_PRINTLN(INIT_MSG_SUCCESS);
    this->_initialized = true;
    return true;
}

/**
 * @brief Check if we have been initialized
 * @return True if we have been initialized
 */
bool WirelessRadio::is_initialized() {
    return this->_initialized;
}

/**
 * @brief Set the RFM95 Transmission Output Power
 * @param[in] tx_power: TX Power, in dBm. -1 to 14 dBm if using pa_boost, else 5 to 23 dBm
 * @param[in] pa_boost: Use PA_BOOST, default false
 */
void WirelessRadio::set_tx_power(int8_t tx_power, bool pa_boost) {
    rf95_radio.setTxPower(tx_power, pa_boost);
}

/**
 * @brief Helper function to set up the RFM95 transceiver
 * @param[in] config_choice: RH_RF95::ModemConfigChoice
 * @param[in] freq_center: Center frequency in MHz
 * @param[in] tx_pwr: Transmit power in dBm
 * @return True if setup was a success
 */
bool WirelessRadio::rfm95_easy_init(RH_RF95::ModemConfigChoice config_choice, float freq_center, int8_t tx_pwr) {
    pinMode(PIN_RFM95_RESET, OUTPUT);
    digitalWrite(PIN_RFM95_RESET, HIGH);
    delay(10);
    digitalWrite(PIN_RFM95_RESET, LOW);
    delay(10);
    digitalWrite(PIN_RFM95_RESET, HIGH);
    delay(10);

    if (!rf95_radio.init())
        return false;

    rf95_radio.setModemConfig(config_choice);
    rf95_radio.setFrequency(freq_center);
    this->set_tx_power(tx_pwr);

    return true;
}

/**
 * @brief Convert RSSI to dBm
 * @param[in] rssi: The RSSI value to convert
 * @return Input RSSI to dBm
 */
int WirelessRadio::rfm95_rssi_to_dbm(int rssi) {
    return -137 + rssi;
}

/**
 * @brief Get the last receive transmission dBm value
 * @return Last receive transmission dBm value
 */
int WirelessRadio::get_last_dbm() {
    return this->rfm95_rssi_to_dbm(rf95_radio.lastRssi());
}

/**
 * @brief Things that need to be done after a TX for some reason
 */
void WirelessRadio::do_weird_things_after_tx() {
    // #justRF95things <3
    rf95_radio.setModeRx();
    rf95_radio.force_read();

    if (! rf95_radio.waitAvailableTimeout(1))
        return;

    uint8_t _tmp_buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(_tmp_buf);

    rf95_radio.recv(_tmp_buf, &len);
}

/**
 * @brief Send a static test message
 * @return True if the message was sent successfully
 */
bool WirelessRadio::send_test() {
    uint8_t d[] = "Hello";
    return this->send_raw_data(d, sizeof(d));
}

/**
 * @brief Get the last metadata that was received with recv_mesh_or_timeout
 * @param[out] src: Node id that the last message originated from
 * @param[out] dest: Node id that the last message was going to
 * @param[out] id: ID (index) of the last message
 */
void WirelessRadio::get_last_mesh_metadata(uint8_t *src, uint8_t *dest, uint8_t *id) {
    *src = this->_last_mesh_src;
    *dest = this->_last_mesh_dest;
    *id = this->_last_mesh_id;
}

/**
 * @brief Wirelessly send data (mesh message)
 * @param[in] data: Pointer to the data to be sent
 * @param[in] data_length: Length of the data to be sent
 * @param[in] dest_id: Destination id of the node in the mesh
 * @return True if the data was sent successfully
 */
bool WirelessRadio::send_mesh_data(uint8_t *data, int data_length, uint8_t dest_id) {
    DEBUG_PRINT(F("Send mesh: "));
    uint8_t buf_byte;
    for (uint8_t i = 0; i < data_length; i++) {
        buf_byte = data[i];
        if (buf_byte <= 0xF) {
            DEBUG_PRINT(F("0")); // Print a leading zero
        }
        DEBUG_PRINT(buf_byte, HEX);
    }

    DEBUG_PRINT(F(", "));
    DEBUG_PRINT(data_length);
    DEBUG_PRINT(F(" RAM: "));
    DEBUG_PRINTLN(freeMemory());

    if ((buf_byte = this->mesh_manager->sendtoWait(data, data_length, dest_id)) != RH_ROUTER_ERROR_NONE) {
        DEBUG_PRINTLN(F("Manager error: "));
        DEBUG_PRINTLN(buf_byte);
        return false;
    }

    return true;
}

/**
 * @brief Wirelessly send data (raw message)
 * @param[in] data: Pointer to the data to be sent
 * @param[in] data_length: Length of the data to be sent
 * @return True if the data was sent successfully
 */
bool WirelessRadio::send_raw_data(uint8_t *data, int data_length) {
    DEBUG_PRINT(F("Send raw: "));
    uint8_t buf_byte;
    for (uint8_t i = 0; i < data_length; i++) {
        buf_byte = data[i];
        if (buf_byte <= 0xF) {
            DEBUG_PRINT(F("0")); // Print a leading zero
        }
        DEBUG_PRINT(buf_byte, HEX);
    }

    DEBUG_PRINT(F(", "));
    DEBUG_PRINTLN(data_length);

    if (!rf95_radio.send((const uint8_t *)data, data_length)) {
        return false;
    }

    if (!rf95_radio.waitPacketSent(TX_TIMEOUT)) {
        DEBUG_PRINTLN(TX_TIMEOUT_MSG);
        return false;
    }

    this->do_weird_things_after_tx();

    return true;
}

/**
 * @brief Receive a message or timeout (mesh message)
 * @return True if a message was received
 */
bool WirelessRadio::recv_mesh_or_timeout() {
    uint8_t len = sizeof(this->_recv_buf);
    uint8_t src, dest, id;

    if (! this->mesh_manager->recvfromAckTimeout(this->_recv_buf, &len, RX_TIMEOUT, &src, &dest, &id)) {
        return false;
    }

    DEBUG_PRINT(F("Received ["));
    DEBUG_PRINT(len);
    DEBUG_PRINT(F("]: "))
    uint8_t buf_byte;
    for (uint8_t i = 0; i < len; i++) {
        buf_byte = this->_recv_buf[i];
        if (buf_byte <= 0xF) {
            DEBUG_PRINT(F("0")); // Print a leading zero
        }
        DEBUG_PRINT(buf_byte, HEX);
    }

    DEBUG_PRINT(F(" @ "));
    DEBUG_PRINT(this->get_last_dbm());
    DEBUG_PRINTLN(F("dBm"));

    this->_last_mesh_src = src;
    this->_last_mesh_dest = dest;
    this->_last_mesh_id = id;

    DEBUG_PRINT(F("S: ")); DEBUG_PRINT(src);
    DEBUG_PRINT(F("\tD: ")); DEBUG_PRINT(dest);
    DEBUG_PRINT(F("\tI: ")); DEBUG_PRINT(id);
    DEBUG_PRINT(F("\tRAM: ")); DEBUG_PRINTLN(freeMemory());

    DEBUG_PRINTLN((char*)this->_recv_buf);

    this->mesh_manager->recvfromAckTimeout(this->_recv_buf, &len, 10); // Do a small recv after to flush or something?

    return true;
}

/**
 * @brief Receive a message or timeout (raw message)
 * @return True if a message was received
 */
bool WirelessRadio::recv_raw_or_timeout() {
    if (! rf95_radio.waitAvailableTimeout(RX_TIMEOUT)) {
        return false;
    }

    uint8_t len = sizeof(this->_recv_buf);

    memset(this->_recv_buf, 0, sizeof(this->_recv_buf)); // Zero out our receiver buffer
    if (! rf95_radio.recv(this->_recv_buf, &len)) {
        DEBUG_PRINTLN(F("Receive failed"));
        return false;
    }

    DEBUG_PRINT(F("Received ["));
    DEBUG_PRINT(len);
    DEBUG_PRINT(F("]: "))
    uint8_t buf_byte;
    for (uint8_t i = 0; i < len; i++) {
        buf_byte = this->_recv_buf[i];
        if (buf_byte <= 0xF) {
            DEBUG_PRINT(F("0")); // Print a leading zero
        }
        DEBUG_PRINT(buf_byte, HEX);
    }

    DEBUG_PRINT(F(" @ "));
    DEBUG_PRINT(this->get_last_dbm());
    DEBUG_PRINTLN(F("dBm"));

    DEBUG_PRINTLN((char*)this->_recv_buf);

    return true;
}

/**
 * @brief A function to pass into wdt_sleep_for of what to call before going to sleep.
 * Needed for some reason with the RF95
 */
void _sleep_precall_set_cad() {
    rf95_radio.setModeCAD();
}

/**
 * @brief Set the radio into CAD, and power down.
 * If a CAD interrupt is generated, return the amount of sleep time left
 * @param[in] sleep_time: The amount of time to sleep
 * @return Time remaining when a CAD is generated (0 if no CAD was generated)
 */
uint16_t WirelessRadio::power_down_until_cad(uint16_t sleep_time) {
    rf95_radio.setModeCAD();
    return wdt_sleep_for(&_sleep_precall_set_cad, sleep_time);
}

/**
 * @brief Set the radio into CAD mode
 */
void WirelessRadio::set_cad_interrupt() {
    rf95_radio.setModeCAD();
}

/**
 * @brief Receive a message after CAD mode (mesh messages)
 * @return True if a message was able to be received
 */
bool WirelessRadio::recv_mesh_after_cad() {
    rf95_radio.setModeRx();
    rf95_radio.force_read();
    return this->recv_mesh_or_timeout();
}

/**
 * @brief Receive a message after CAD mode (raw messages)
 * @return True if a message was able to be received
 */
bool WirelessRadio::recv_raw_after_cad() {
    rf95_radio.setModeRx();
    rf95_radio.force_read();
    return this->recv_raw_or_timeout();
}

/**
 * @brief Return the last message type
 * @return The last packet type
 */
PacketType WirelessRadio::last_message_type() {
    return (PacketType) this->_recv_buf[0];
}

/**
 * @brief Send a SEND_DATA message
 * @param[in] dest_mesh_id: Destination node id
 * @param[in] send_time: Sender timestamp
 * @param[in] num_sensor_bytes: Number of following bytes of data
 * @param[in] data_buf: Pointer to array of size [num_sensor_bytes]
 * @return True if a message was able to be sent
 */
bool WirelessRadio::send_SEND_DATA(uint8_t dest_mesh_id, time_t send_time, uint8_t num_sensor_bytes, uint8_t *data_buf) {
    uint8_t memsize =
            sizeof(PacketType) +
            sizeof(send_time) +
            sizeof(num_sensor_bytes) +
            (num_sensor_bytes * sizeof(uint8_t));
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    buf[0] = PacketType::SEND_DATA;
    buf[1] = time_to_uint8(send_time, 0);
    buf[2] = time_to_uint8(send_time, 1);
    buf[3] = time_to_uint8(send_time, 2);
    buf[4] = time_to_uint8(send_time, 3);
    buf[5] = num_sensor_bytes;

    for (uint8_t i = 6; i < memsize; i++)  // Loop through starting from the sixth byte
        buf[i] = data_buf[i - 6];

    bool success = this->send_mesh_data(buf, memsize, dest_mesh_id);
    free(buf);
    return success;
}

/**
 * @brief Unpack a SEND_DATA message
 * @param[out] send_time: Sender timestamp
 * @param[out] num_sensor_bytes: Number of following bytes of data
 * @param[out] data_buf: Pointer to array of size [num_sensor_bytes]
 * @return True if last packet was valid
 */
bool WirelessRadio::unpack_SEND_DATA(time_t *send_time, uint8_t *num_sensor_bytes, uint8_t **data_buf) {
    DEBUG_PRINTLN(F("Unpacking SEND_DATA"));
    if (this->_recv_buf[0] != PacketType::SEND_DATA) {
        DEBUG_PRINTLN(F("Invalid. Packet is not SEND_DATA"));
        return false;
    }

    *send_time = uint8_to_time(this->_recv_buf[1],
                               this->_recv_buf[2],
                               this->_recv_buf[3],
                               this->_recv_buf[4]);
    *num_sensor_bytes = this->_recv_buf[5];
    for (uint8_t i = 6; (i - 6) < (*num_sensor_bytes); i++) {  // Loop through starting from the sixth byte
        (*data_buf)[i - 6] = this->_recv_buf[i];
    }

    return true;
}

/**
 * @brief Send a NEW_NODE message
 * @param[in] dest_mesh_id: Destination node id (usually 0xFF for multicast)
 * @return True if a message was able to be sent
 */
bool WirelessRadio::send_NEW_NODE(uint8_t dest_mesh_id) {
    uint8_t memsize =
            sizeof(PacketType);
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    buf[0] = PacketType::NEW_NODE;

    bool success = this->send_mesh_data(buf, memsize, dest_mesh_id);
    free(buf);
    return success;
}

/**
 * @brief Unpack a NEW_NODE message
 * @return True if the last packet was valid
 */
bool WirelessRadio::unpack_NEW_NODE() {
    DEBUG_PRINTLN(F("Unpacking NEW_NODE"));
    if (this->_recv_buf[0] != PacketType::NEW_NODE) {
        DEBUG_PRINTLN(F("Invalid. Packet is not NEW_NODE"));
        return false;
    }

    return true;
}

/**
 * @brief Send a HUB_WELCOME message
 * @param[in] dest_mesh_id: Destination node id
 * @param[in] hub_mesh_id: This hub's mesh id
 * @param[in] timestamp: The current time
 * @return True if a message was able to be sent
 */
bool WirelessRadio::send_HUB_WELCOME(uint8_t dest_mesh_id, uint8_t hub_mesh_id, time_t timestamp) {
    uint8_t memsize =
            sizeof(PacketType) +
            sizeof(hub_mesh_id) +
            sizeof(timestamp);
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    buf[0] = PacketType::HUB_WELCOME;
    buf[1] = hub_mesh_id;
    buf[2] = time_to_uint8(timestamp, 0);
    buf[3] = time_to_uint8(timestamp, 1);
    buf[4] = time_to_uint8(timestamp, 2);
    buf[5] = time_to_uint8(timestamp, 3);

    bool success = this->send_mesh_data(buf, memsize, dest_mesh_id);
    free(buf);
    return success;
}

/**
 * @brief Unpack a HUB_WELCOME message
 * @param[out] hub_mesh_id: The hub's mesh id
 * @param[out] timestamp: The hub's current time
 * @return True if the last packet was valid
 */
bool WirelessRadio::unpack_HUB_WELCOME(uint8_t *hub_mesh_id, time_t *timestamp) {
    DEBUG_PRINTLN(F("Unpacking HUB_WELCOME"));
    if (this->_recv_buf[0] != PacketType::HUB_WELCOME) {
        DEBUG_PRINTLN(F("Invalid. Packet is not HUB_WELCOME"));
        return false;
    }

    *hub_mesh_id = this->_recv_buf[1];
    *timestamp = uint8_to_time(this->_recv_buf[2],
                               this->_recv_buf[3],
                               this->_recv_buf[4],
                               this->_recv_buf[5]);

    return true;
}

/**
 * @brief Send a ADJUST_SAMP message
 * @param[in] dest_mesh_id: Destination node id
 * @param[in] samp_rate: Sample rate
 * @return True if a message was able to be sent
 */
bool WirelessRadio::send_ADJUST_SAMP(uint8_t dest_mesh_id, uint8_t samp_rate) {
    uint8_t memsize =
            sizeof(PacketType) +
            sizeof(samp_rate);
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    buf[0] = PacketType::ADJUST_SAMP;
    buf[1] = samp_rate;

    bool success = this->send_mesh_data(buf, memsize, dest_mesh_id);
    free(buf);
    return success;
}

/**
 * @brief Unpack a ADJUST_SAMP message
 * @param[out] samp_rate: Sample rate
 * @return True if last packet was valid
 */
bool WirelessRadio::unpack_ADJUST_SAMP(uint8_t *samp_rate) {
    DEBUG_PRINTLN(F("Unpacking ADJUST_SAMP"));
    if (this->_recv_buf[0] != PacketType::ADJUST_SAMP) {
        DEBUG_PRINTLN(F("Invalid. Packet is not ADJUST_SAMP"));
        return false;
    }

    *samp_rate = this->_recv_buf[1];

    return true;
}

/**
 * @brief Send a ADJUST_TXPWR message
 * @param[in] dest_mesh_id: Destination mesh id
 * @param[in] tx_power: TX Power
 * @return True if a message was able to be sent
 */
bool WirelessRadio::send_ADJUST_TXPWR(uint8_t dest_mesh_id, int8_t tx_power) {
    uint8_t memsize =
            sizeof(PacketType) +
            sizeof(tx_power);
    uint8_t *buf = (uint8_t *) calloc(memsize, sizeof(uint8_t));
    buf[0] = PacketType::ADJUST_TXPWR;
    buf[1] = tx_power;

    bool success = this->send_mesh_data(buf, memsize, dest_mesh_id);
    free(buf);
    return success;
}

/**
 * @brief Unpack a ADJUST_TXPWR message
 * @param[out] tx_power: TX Power
 * @return True if last packet was valid
 */
bool WirelessRadio::unpack_ADJUST_TXPWR(int8_t *tx_power) {
    DEBUG_PRINTLN(F("Unpacking ADJUST_TXPWR"));
    if (this->_recv_buf[0] != PacketType::ADJUST_TXPWR) {
        DEBUG_PRINTLN(F("Invalid. Packet is not ADJUST_TXPWR"));
        return false;
    }

    *tx_power = this->_recv_buf[1];

    return true;
}
