#include "helpers.h"

/**
 * @brief Map a floating point value from one range to another
 * @param[in] x: Value to map
 * @param[in] in_min: Input min
 * @param[in] in_max: Input max
 * @param[in] out_min: Output min
 * @param[in] out_max Output max
 * @return The mapped floating point value
 */
float mapf(float x, float in_min, float in_max, float out_min, float out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/**
 * @brief Convert eight bits of a float into a uint8_t
 * @param[in] value: The time to convert
 * @param[in] byte_number: Which bits to slice
 * @return A uint8_t slice of the input time_t
 */
uint8_t float_to_uint8(float value, uint8_t byte_number) {
    union {
        float _union_f;
        uint8_t _union_b[4];
    };

    _union_f = value;

    return _union_b[byte_number];
}

/**
 * @brief Convert four uint8_t values into a float
 * @param[in] byte_0: The first byte
 * @param[in] byte_1: The second byte
 * @param[in] byte_2: The third byte
 * @param[in] byte_3: The fourth byte
 * @return float composed of the input bytes
 */
float uint8_to_float(uint8_t byte_0, uint8_t byte_1, uint8_t byte_2, uint8_t byte_3) {
    union {
        float _union_f;
        uint8_t _union_b[4];
    };

    _union_b[0] = byte_0;
    _union_b[1] = byte_1;
    _union_b[2] = byte_2;
    _union_b[3] = byte_3;

    return _union_f;
}

/**
 * @brief Convert eight bits of a time_t into a uint8_t
 * @param[in] value: The time to convert
 * @param[in] byte_number: Which bits to slice
 * @return A uint8_t slice of the input time_t
 */
uint8_t time_to_uint8(time_t value, uint8_t byte_number) {
    switch (byte_number) {
        case 3: return (uint8_t) ((value >> 24) & 0xFF);
        case 2: return (uint8_t) ((value >> 16) & 0xFF);
        case 1: return (uint8_t) ((value >> 8) & 0xFF);
        case 0: return (uint8_t) ((value >> 0) & 0xFF);
        default: DEBUG_PRINTLN(F("Invalid byte #")); return 0; // Invalid
    }
}

/**
 * @brief Convert four uint8_t values into a time_t
 * @param[in] byte_0: The first byte
 * @param[in] byte_1: The second byte
 * @param[in] byte_2: The third byte
 * @param[in] byte_3: The fourth byte
 * @return time_t composed of the input bytes
 */
time_t uint8_to_time(uint8_t byte_0, uint8_t byte_1, uint8_t byte_2, uint8_t byte_3) {
    return (time_t) (((long) byte_3 << 24) | ((long) byte_2 << 16) | ((long) byte_1 << 8) | ((long) byte_0 << 0));
}

/**
 * @brief Convert the high/low bits of a uint16_t value into a uint8_t
 * @param[in] value: The value to convert
 * @param[in] high_bits: Return the high or low bits
 * @return The converted uint8_t value
 */
uint8_t uint16_to_uint8(uint16_t value, bool high_bits) {
    if (high_bits) {
        return (uint8_t) (value >> 8); // High bits
    } else {
        return (uint8_t) (value & 0xFF); // Low bits
    }
}

/**
 * @brief Convert two uint8_t values into a uint16_value
 * @param[in] low_bits: The low bits
 * @param[in] high_bits: THe high bits
 * @return Converted uint16_t value
 */
uint16_t uint8_to_uint16(uint8_t low_bits, uint8_t high_bits) {
    return (uint16_t) ((high_bits << 8) | (low_bits << 0));
}

/**
 * @brief Convert 4 bytes to a float value
 * @param[in] b: Pointer to bytes to convert
 * @return float of the bytes
 */
float bytes_to_float(uint8_t *b) {
    union {
        float _union_f;
        uint8_t _union_b[4];
    };

    _union_b[0] = b[0];
    _union_b[1] = b[1];
    _union_b[2] = b[2];
    _union_b[3] = b[3];

    return _union_f;
}

/**
 * @brief Convert 4 bytes to a time_t
 * @param[in] b: Pointer to bytes to convert
 * @return time_t of the bytes
 */
time_t bytes_to_time(uint8_t *b) {
    union {
        time_t _union_t;
        uint8_t _union_b[4];
    };

    _union_b[0] = b[0];
    _union_b[1] = b[1];
    _union_b[2] = b[2];
    _union_b[3] = b[3];

    return _union_t;
}

/**
 * @brief Flush the ATMega Serial buffer
 */
void flush_serial_buffer() {
    if (!Serial) {
        return;
    }
    while(Serial.available()) {
        Serial.read();
        delay(10);
    }
}

/**
 * @brief Check if an integer is between two other integers
 * @param[in] low: Low value
 * @param[in] x: Value to check
 * @param[in] high: High value
 * @return True if low <= x <= high
 */
bool between(long low, long x, long high) {
    return (low <= x) && (x <= high);
}

/**
 * @brief This function does nothing
 */
void do_nothing() {
    return;
}