#ifndef NODE_FIRMWARE_WIRELESSRADIO_H
#define NODE_FIRMWARE_WIRELESSRADIO_H

#define RH_RF95_MAX_MESSAGE_LEN (30)

#include <Arduino.h>
#include <RHMesh.h>
#include <RH_RF95.h>
#include <Time.h>
#include <MemoryFree.h>
#include "HardwareDefinition.h"
#include "DebugSerial.h"
#include "helpers.h"
#include "WdtSleep.h"

#define RFM95_FREQ (915.0) // MHz
#define RFM95_TX_TYPE (RH_RF95::Bw125Cr45Sf128)
#define TX_TIMEOUT (5000)
#define TX_TIMEOUT_MSG (F("TX Timeout"))
#define RX_TIMEOUT (2000)
#define RX_TIMEOUT_MSG (F("RX Timeout"))

enum PacketType : uint8_t {
    SEND_DATA     = 0x01,
    NEW_NODE      = 0x02,
    HUB_WELCOME   = 0x03,
    ADJUST_SAMP   = 0xF1,
    ADJUST_TXPWR  = 0xF2
};

class WirelessRadio {
public:
    bool initialize(uint8_t, uint8_t);
    void set_tx_power(int8_t, bool = false);
    int rfm95_rssi_to_dbm(int);
    int get_last_dbm();
    void get_last_mesh_metadata(uint8_t *, uint8_t *, uint8_t *);
    bool is_initialized();
    bool send_mesh_data(uint8_t *, int, uint8_t);
    bool send_raw_data(uint8_t *, int);
    bool send_test();
    bool recv_mesh_or_timeout();
    bool recv_raw_or_timeout();
    void set_cad_interrupt();
    bool recv_mesh_after_cad();
    bool recv_raw_after_cad();
    uint16_t power_down_until_cad(uint16_t);
    PacketType last_message_type();

    bool   send_SEND_DATA(uint8_t, time_t  , uint8_t  , uint8_t * );
    bool unpack_SEND_DATA(         time_t *, uint8_t *, uint8_t **);
    bool   send_NEW_NODE(uint8_t);
    bool unpack_NEW_NODE(       );
    bool   send_HUB_WELCOME(uint8_t, uint8_t  , time_t  );
    bool unpack_HUB_WELCOME(         uint8_t *, time_t *);
    bool   send_ADJUST_SAMP(uint8_t, uint8_t  );
    bool unpack_ADJUST_SAMP(         uint8_t *);
    bool   send_ADJUST_TXPWR(uint8_t, int8_t  );
    bool unpack_ADJUST_TXPWR(         int8_t *);

private:
    bool rfm95_easy_init(RH_RF95::ModemConfigChoice, float, int8_t);
    void do_weird_things_after_tx();

    RHMesh *mesh_manager;
    uint8_t _last_mesh_src;
    uint8_t _last_mesh_dest;
    uint8_t _last_mesh_id;
    uint8_t _recv_buf[RH_RF95_MAX_MESSAGE_LEN];
    bool _initialized = false;
};

#endif //NODE_FIRMWARE_WIRELESSRADIO_H
