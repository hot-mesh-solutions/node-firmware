#ifndef NODE_FIRMWARE_DEBUGINTERFACE_H
#define NODE_FIRMWARE_DEBUGINTERFACE_H

//#define BUILD_DEBUG_INTERFACE

#include <Arduino.h>
#include <MemoryFree.h>
#include <time.h>
#include "DebugSerial.h"
#include "helpers.h"
#include "StoredData.h"
#include "ExternalSensor.h"
#include "StatusLed.h"
#include "WirelessRadio.h"
#include "ExternalAdc.h"
#include "ExternalRtc.h"
#include "WdtSleep.h"

#ifdef BUILD_DEBUG_INTERFACE

#define MENU_WAIT_AVAILABLE_TIME (100)
#define AUTO_UPDATE_DELAY (250)

#define PROMPT_TEXT (F("?> "))
#define EXIT_OPTION_TEXT (F(" | (e)xit"))

#define EXIT_MSG F("Exiting")
#define INVALID_MSG F("Invalid")

#define PAUSE_TEXT F(\
"[(c)ontinue] | (p)rompt")

#define MAIN_MENU_TEXT F(\
"Main: (g)eneral | (s)ettings | e(x)t sensor | (r)adio | (a)dc | r(t)c")

#define SETTINGS_MENU_TEXT F(\
"Settings: (i)nitialize | (p)rint | (s)et SN | (d)ump | (D)ump raw | (!)erase")

#define SPI_ADC_MENU_SCREEN F(\
"ADC: (i)nitialize | (r)ead all | reference (v)oltage")

#define SPI_RTC_MENU_SCREEN F(\
"RTC: (i)nitialize | (g)et time | (S)et to compiled time")

#define EXTERNAL_SENSOR_MENU_SCREEN F(\
"Ext Sensor: (t)oggle | (c)heck")

#define RADIO_MENU_SCREEN F(\
"Radio: (i)nitialize | (s)end message | (t)est send | (r)ecv or timeout")

#define GENERAL_STATUS_MENU_SCREEN F(\
"Status: (a)uto update | LED (1,2,3,4,5) | (s)tate")

#define GENERAL_STATUS_TEXT PSTR(\
"Modules | Radio: %i | Settings: %i | E_ADC: %i | E_RTC: %i\r\n" \
"SN: %04X\tMeshid: %i\tState: %02X\r\n" \
"Time: %s (%lu)\r\n" \
"LED PWM: %i\r\n" \
"EAnalog| Solar\t\tBatt\t\tMainC\t\tExtC\r\n" \
"         %s\t\t%s\t\t%s\t\t%s\r\n")

#endif // BUILD_DEBUG_INTERFACE

class DebugInterface {
private:
#ifdef BUILD_DEBUG_INTERFACE
    void print_status();
    void send_wireless_message();

    uint8_t *cur_state;
    StoredData *stored_data;
    ExternalSensor *external_sensor;
    StatusLed *status_led;
    WirelessRadio *wireless_radio;
    ExternalAdc *external_adc;
    ExternalRtc *external_rtc;
#endif // BUILD_DEBUG_INTERFACE
public:
    DebugInterface(
            uint8_t *current_state,
            StoredData *,
            ExternalSensor *,
            StatusLed *,
            WirelessRadio *,
            ExternalAdc *,
            ExternalRtc *);
    bool pause(uint8_t);
#ifdef BUILD_DEBUG_INTERFACE
    void main_menu();
    void settings_menu();
    void adc_menu();
    void rtc_menu();
    void external_sensor_menu();
    void radio_menu();
    void general_status_menu();
    void set_serial_number();
    void set_state();
#endif // BUILD_DEBUG_INTERFACE
};

#endif //NODE_FIRMWARE_DEBUGINTERFACE_H
