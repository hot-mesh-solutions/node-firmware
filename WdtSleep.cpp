#include "WdtSleep.h"

volatile bool _WDT_TRIGGERED = false;

/**
 * @brief ISR interrupt for the watchdog
 */
ISR(WDT_vect) { // Handles the Watchdog Time-out Interrupt
    _WDT_TRIGGERED = true;
}

/**
 * @brief Reset the WDT, then turn off all peripherals
 */
void go_to_sleep(void) {
    byte adcsra = ADCSRA;          //save the ADC Control and Status Register A
    ADCSRA = 0;                    //disable the ADC
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        _WDT_TRIGGERED = false;
        sleep_enable();
        sleep_bod_disable();       //disable brown-out detection (saves 20-25µA)
    }
    sleep_cpu();                   //go to sleep
    sleep_disable();               //wake up here
    ADCSRA = adcsra;               //restore ADCSRA
}

/**
 * @brief Change the WDT sleep time
 * @param[in] wdt_time: Bit value of the WDTCSR register
 */
void change_wdt_sleep_time(int wdt_time) {
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        wdt_reset();
        MCUSR &= ~_BV(WDRF);                            //clear WDRF
        WDTCSR |= _BV(WDCE) | _BV(WDE);                 //enable WDTCSR change
        WDTCSR =  _BV(WDIE) | wdt_time;
    }
}

/**
 * @brief Sleep for `seconds` using the Watch Dog Timer
 * @param[in]function_precall: Function that should be called before setting the AVR into sleep mode
 * @param[in] seconds: How long to sleep for
 * @return The number of seconds left if sleep was interrupted by a non-WDT interrupt
 */
uint16_t wdt_sleep_for(void (*function_precall)(), uint16_t seconds) {
    int32_t countdown = seconds;
    DEBUG_PRINT(F("WDTSleep "));
    DEBUG_PRINTLN(seconds);
    delay(10);

    while (countdown > 0) {
             if (countdown >= 8) { change_wdt_sleep_time(WDT_8S); countdown -= 8; }
        else if (countdown >= 4) { change_wdt_sleep_time(WDT_4S); countdown -= 4; }
        else if (countdown >= 2) { change_wdt_sleep_time(WDT_2S); countdown -= 2; }
        else if (countdown >= 1) { change_wdt_sleep_time(WDT_1S); countdown -= 1; }

        function_precall();
        go_to_sleep();

        if (! _WDT_TRIGGERED) {
            return countdown < 0 ? 0 : (uint16_t) countdown;
        }
    }

    _WDT_TRIGGERED = true; // Clear the status

    return 0;
}
