#ifndef NODE_FIRMWARE_WDTSLEEP_H
#define NODE_FIRMWARE_WDTSLEEP_H

#include <Arduino.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include "DebugSerial.h"

#define WDT_500MS (            _BV(WDP2) |             _BV(WDP0))
#define WDT_1S    (            _BV(WDP2) | _BV(WDP1)            )
#define WDT_2S    (            _BV(WDP2) | _BV(WDP1) | _BV(WDP0))
#define WDT_4S    (_BV(WDP3)                                    )
#define WDT_8S    (_BV(WDP3) |                         _BV(WDP0))

void go_to_sleep(void);
void change_wdt_sleep_time(int);
uint16_t wdt_sleep_for(void (*)(), uint16_t);

#endif //NODE_FIRMWARE_WDTSLEEP_H
