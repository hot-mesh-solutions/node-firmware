var _debug_interface_8h =
[
    [ "DebugInterface", "class_debug_interface.html", "class_debug_interface" ],
    [ "AUTO_UPDATE_DELAY", "_debug_interface_8h.html#acb81aa5e559650e6da98d4a601890de6", null ],
    [ "EXIT_MSG", "_debug_interface_8h.html#afd9fca35bfcd8ac6803373af0aa1fcff", null ],
    [ "EXIT_OPTION_TEXT", "_debug_interface_8h.html#a2ebd4d11b6bcf5dcf9d6df76c254393a", null ],
    [ "EXTERNAL_SENSOR_MENU_SCREEN", "_debug_interface_8h.html#aa3e2b776d339b03d0c50df07b65acbbe", null ],
    [ "GENERAL_STATUS_MENU_SCREEN", "_debug_interface_8h.html#aa44173c4145c48db8b6b7240479836a9", null ],
    [ "GENERAL_STATUS_TEXT", "_debug_interface_8h.html#a46d44a2f08f368642c14233ba0649347", null ],
    [ "INPUT_BUF_SIZE", "_debug_interface_8h.html#a8f53190540130db759820c0d9ab78f60", null ],
    [ "INVALID_MSG", "_debug_interface_8h.html#a9a15cb3f0a4342888e670357d73a041d", null ],
    [ "MAIN_MENU_TEXT", "_debug_interface_8h.html#a738ee16f08823374a02b0dfdf3c6bbcc", null ],
    [ "MENU_WAIT_AVAILABLE_TIME", "_debug_interface_8h.html#a292ae38f9d9364fd5ac70dad9cdecca1", null ],
    [ "PAUSE_TEXT", "_debug_interface_8h.html#a9e873fa6547841f8143e810e80210ae6", null ],
    [ "PROMPT_TEXT", "_debug_interface_8h.html#af601adb3c94c994505dfeee69fef8dbb", null ],
    [ "RADIO_MENU_SCREEN", "_debug_interface_8h.html#aca85670989194c671a6fdc2442e07635", null ],
    [ "SETTINGS_MENU_TEXT", "_debug_interface_8h.html#acee567ab789beb4bf78c63ae5c83eb84", null ],
    [ "SPI_ADC_MENU_SCREEN", "_debug_interface_8h.html#aef32987baf42251a3d221db41f31f772", null ],
    [ "SPI_RTC_MENU_SCREEN", "_debug_interface_8h.html#aa99153ea69664c8a28a878d6078c558f", null ]
];