var _external_sensor_8h =
[
    [ "ExternalSensor", "class_external_sensor.html", "class_external_sensor" ],
    [ "SENSOR_WAIT_ATTEMPTS", "_external_sensor_8h.html#aafe4fa7269ca96b3343f79a9147ec8aa", null ],
    [ "SENSOR_WAIT_DELAY", "_external_sensor_8h.html#a7d740c3c976bcbd18756e59099d31cc2", null ],
    [ "SensorType", "_external_sensor_8h.html#a87ca7b4499ea761999a872674eba32af", [
      [ "BATTERY_VOLTAGE", "_external_sensor_8h.html#a87ca7b4499ea761999a872674eba32afa33dfae05581973af118a10481e139aba", null ],
      [ "SOLAR_VOLTAGE", "_external_sensor_8h.html#a87ca7b4499ea761999a872674eba32afafdf78c845fdfa1977a7eec5cfb3944c6", null ],
      [ "TEMPERATURE", "_external_sensor_8h.html#a87ca7b4499ea761999a872674eba32afac4ae6787ff1d8b2d1cf0ae9aa696e56c", null ]
    ] ]
];