var struct_sensor_packet =
[
    [ "from_node_number", "struct_sensor_packet.html#a2879eb1af77c202eb9292db29b451ced", null ],
    [ "packet_type", "struct_sensor_packet.html#a970b8d8e479624ea908fee81bc94dc6a", null ],
    [ "serial_number", "struct_sensor_packet.html#a3a5cf8ed83313a5bf40c25391de02a56", null ],
    [ "temperature", "struct_sensor_packet.html#a8948b3707c8f3486e67c55f59e87e42b", null ],
    [ "timestamp", "struct_sensor_packet.html#acb4b68939ef67985dc4735d4bf95a1c4", null ],
    [ "to_node_number", "struct_sensor_packet.html#a9ec3bced10678cde2cb70b7f3331ebba", null ]
];