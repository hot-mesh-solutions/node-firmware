var searchData=
[
  ['sensor_5fdata',['sensor_data',['../struct_sensordata_struct.html#ab9cd0e5e4de33e5fd3514dadaec90b29',1,'SensordataStruct']]],
  ['sensor_5fdata_5faddr',['sensor_data_addr',['../class_stored_data.html#ac52bf998523c7ab509dd9aa04ea10fea',1,'StoredData']]],
  ['sensor_5ftype',['sensor_type',['../struct_sensordata_struct.html#afd7f490302cf2b70bbfa35bb016e593d',1,'SensordataStruct']]],
  ['serial_5fnumber',['serial_number',['../struct_settings_struct.html#a96d6eec0cd43342a4ec2488c745821bf',1,'SettingsStruct']]],
  ['settings',['settings',['../class_stored_data.html#afbe4ee0f51e697ab979380a683e75b69',1,'StoredData']]],
  ['sleep_5ftime',['sleep_time',['../struct_settings_struct.html#af34b5d544fef5cfe33f473220417282e',1,'SettingsStruct']]],
  ['status_5fled',['status_led',['../main_8cpp.html#a406fc1f1307853ef916c16e58c614023',1,'main.cpp']]],
  ['stored_5fdata',['stored_data',['../main_8cpp.html#a5c67ce1dede3f2911ffc9c0fb470518a',1,'main.cpp']]]
];
