var searchData=
[
  ['calculate_5fchecksum',['calculate_checksum',['../class_stored_data.html#a7b71a9db19c85fde7e44864fc2048154',1,'StoredData']]],
  ['calculate_5fsettings_5fchecksum',['calculate_settings_checksum',['../class_stored_data.html#a411a6a4cf64ae7894b3e1ec7713cb6e4',1,'StoredData']]],
  ['change_5fwdt_5fsleep_5ftime',['change_wdt_sleep_time',['../_wdt_sleep_8cpp.html#a49eaabf6297ce6471568d386815fba7c',1,'change_wdt_sleep_time(int wdt_time):&#160;WdtSleep.cpp'],['../_wdt_sleep_8h.html#ac236d6588dc2172a9c8aabfc1af83f1b',1,'change_wdt_sleep_time(int):&#160;WdtSleep.cpp']]],
  ['check_5fcommand_5ftype',['check_command_type',['../class_hub_control.html#a5553343db9adaafe3056eacf886d324f',1,'HubControl']]],
  ['check_5fsensor',['check_sensor',['../class_external_sensor.html#a6fbc7abad5783cc374e0ce0cfa7a4d2a',1,'ExternalSensor']]],
  ['checksum',['checksum',['../struct_settings_struct.html#a48a96de9ce57ab6fee0c0dda23cc8a61',1,'SettingsStruct']]],
  ['computer_5fbaudrate',['COMPUTER_BAUDRATE',['../main_8cpp.html#abf0f581a4c0f1023eacfe1fa159d9c8a',1,'main.cpp']]],
  ['crc16',['CRC16',['../_stored_data_8cpp.html#af7c358c8183aff56ca972b70cd1b2f2f',1,'StoredData.cpp']]],
  ['curr_5fstate',['curr_state',['../main_8cpp.html#a0688506fb04841a594bcbe324d5c2e7b',1,'main.cpp']]]
];
