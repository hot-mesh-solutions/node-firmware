var class_stored_data =
[
    [ "calculate_checksum", "class_stored_data.html#a7b71a9db19c85fde7e44864fc2048154", null ],
    [ "calculate_settings_checksum", "class_stored_data.html#a411a6a4cf64ae7894b3e1ec7713cb6e4", null ],
    [ "dump_eeprom", "class_stored_data.html#a3d8c78d6dc187449d77f1d9d102c8235", null ],
    [ "erase_all", "class_stored_data.html#a47484affe32332bd6de126b0cea7c4e8", null ],
    [ "fill_settings_from", "class_stored_data.html#ae20cf269e014fdfebb1b73c7d6d08870", null ],
    [ "initialize", "class_stored_data.html#a1881f069ad170c9f0cbb60bb0ee47e18", null ],
    [ "is_initialized", "class_stored_data.html#a5fad069e11e37c3b2ac3567b9825afb5", null ],
    [ "print_settings", "class_stored_data.html#aeca52e100afa3e28c388aee352f60ad4", null ],
    [ "print_stored_sensor_data", "class_stored_data.html#abc64404646e2a83bf511f4fc11d97736", null ],
    [ "store_sensor_data", "class_stored_data.html#ab416b0cb61f3be3ade36507c40a4c2b7", null ],
    [ "update_settings", "class_stored_data.html#ad2e9c7fa98048be8dc131977d591f051", null ],
    [ "write_settings_to", "class_stored_data.html#a88aa5e91ba920bebf29d08b2679ef72f", null ],
    [ "sensor_data_addr", "class_stored_data.html#ac52bf998523c7ab509dd9aa04ea10fea", null ],
    [ "settings", "class_stored_data.html#afbe4ee0f51e697ab979380a683e75b69", null ]
];