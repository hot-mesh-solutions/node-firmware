var files_dup =
[
    [ "DebugInterface.cpp", "_debug_interface_8cpp.html", "_debug_interface_8cpp" ],
    [ "DebugInterface.h", "_debug_interface_8h.html", [
      [ "DebugInterface", "class_debug_interface.html", "class_debug_interface" ]
    ] ],
    [ "DebugSerial.cpp", "_debug_serial_8cpp.html", "_debug_serial_8cpp" ],
    [ "DebugSerial.h", "_debug_serial_8h.html", "_debug_serial_8h" ],
    [ "ExternalAdc.cpp", "_external_adc_8cpp.html", "_external_adc_8cpp" ],
    [ "ExternalAdc.h", "_external_adc_8h.html", "_external_adc_8h" ],
    [ "ExternalRtc.cpp", "_external_rtc_8cpp.html", "_external_rtc_8cpp" ],
    [ "ExternalRtc.h", "_external_rtc_8h.html", "_external_rtc_8h" ],
    [ "ExternalSensor.cpp", "_external_sensor_8cpp.html", "_external_sensor_8cpp" ],
    [ "ExternalSensor.h", "_external_sensor_8h.html", "_external_sensor_8h" ],
    [ "HardwareDefinition.h", "_hardware_definition_8h.html", "_hardware_definition_8h" ],
    [ "helpers.cpp", "helpers_8cpp.html", "helpers_8cpp" ],
    [ "helpers.h", "helpers_8h.html", "helpers_8h" ],
    [ "HubControl.cpp", "_hub_control_8cpp.html", null ],
    [ "HubControl.h", "_hub_control_8h.html", "_hub_control_8h" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "StatusLed.cpp", "_status_led_8cpp.html", null ],
    [ "StatusLed.h", "_status_led_8h.html", "_status_led_8h" ],
    [ "StoredData.cpp", "_stored_data_8cpp.html", "_stored_data_8cpp" ],
    [ "StoredData.h", "_stored_data_8h.html", "_stored_data_8h" ],
    [ "WdtSleep.cpp", "_wdt_sleep_8cpp.html", "_wdt_sleep_8cpp" ],
    [ "WdtSleep.h", "_wdt_sleep_8h.html", "_wdt_sleep_8h" ],
    [ "WirelessRadio.cpp", "_wireless_radio_8cpp.html", "_wireless_radio_8cpp" ],
    [ "WirelessRadio.h", "_wireless_radio_8h.html", "_wireless_radio_8h" ]
];