var _hub_control_8h =
[
    [ "HubControl", "class_hub_control.html", "class_hub_control" ],
    [ "TIMEOUT_WARNING", "_hub_control_8h.html#a05331a7d20b392665e16435269dae947", null ],
    [ "UART_TIMEOUT", "_hub_control_8h.html#adfa4c5934f87ff1d1fd9ca34502a20ca", null ],
    [ "HubCommand", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764", [
      [ "INVALID", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764aef2863a469df3ea6871d640e3669a2f2", null ],
      [ "RequestTime", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764a44cbaf60f1fd62545be5ba2005b04d61", null ],
      [ "StoreData", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764a27a25477e2b2977a8015fb8ffd36c357", null ],
      [ "SendAdjTXPwr", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764a84f6e6a660694ab1bb381894273c4ed0", null ],
      [ "SendAdjSamp", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764a8dd7f206fb9e687579232fa5d9af59d2", null ],
      [ "SendWelcoming", "_hub_control_8h.html#a46c8655ee02b8b61ea7ec0944c7ad764a0dc635f8fe3a4234ff900b036cfe279a", null ]
    ] ]
];