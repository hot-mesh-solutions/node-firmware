var annotated_dup =
[
    [ "DebugInterface", "class_debug_interface.html", "class_debug_interface" ],
    [ "ExternalAdc", "class_external_adc.html", "class_external_adc" ],
    [ "ExternalRtc", "class_external_rtc.html", "class_external_rtc" ],
    [ "ExternalSensor", "class_external_sensor.html", "class_external_sensor" ],
    [ "HubControl", "class_hub_control.html", "class_hub_control" ],
    [ "SensordataStruct", "struct_sensordata_struct.html", "struct_sensordata_struct" ],
    [ "SettingsStruct", "struct_settings_struct.html", "struct_settings_struct" ],
    [ "SettingsToByteArr", "union_settings_to_byte_arr.html", "union_settings_to_byte_arr" ],
    [ "StatusLed", "class_status_led.html", "class_status_led" ],
    [ "StoredData", "class_stored_data.html", "class_stored_data" ],
    [ "WirelessRadio", "class_wireless_radio.html", "class_wireless_radio" ]
];