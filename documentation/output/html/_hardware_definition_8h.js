var _hardware_definition_8h =
[
    [ "ATMEGA328P_PU_HARDWARE", "_hardware_definition_8h.html#ad1373c947e10b522e1bc094b67cdd2d7", null ],
    [ "PIN_3V3_DIVIDER", "_hardware_definition_8h.html#a2b8b3fea1d401e255a00e8b364b21720", null ],
    [ "PIN_5V_DIVIDER", "_hardware_definition_8h.html#a50764647340dcb90c40449766e7b0f6c", null ],
    [ "PIN_ADC_CS", "_hardware_definition_8h.html#a0ff997bab2def4bc4ea203809debb414", null ],
    [ "PIN_BACKUP_ADC0", "_hardware_definition_8h.html#ae2edb05ee9ce237cf49c5443e19f2c17", null ],
    [ "PIN_BACKUP_ADC1", "_hardware_definition_8h.html#aa2a883ab3115d57a79af11d40443d7d3", null ],
    [ "PIN_BACKUP_ADC2", "_hardware_definition_8h.html#a432b7d762f1ea9fd42134ad04f9e9b7a", null ],
    [ "PIN_BACKUP_ADC3", "_hardware_definition_8h.html#ab6ed0f6252f7b07504f610719b54643f", null ],
    [ "PIN_BATT_PGOOD", "_hardware_definition_8h.html#abb2f206155988a7f8310e74c450475d9", null ],
    [ "PIN_E_SENSOR_PWR", "_hardware_definition_8h.html#af11c806b85d09f5149c463f50deb3906", null ],
    [ "PIN_RFM95_CS", "_hardware_definition_8h.html#aff3d2e4d9b058767ed8eab278b18d355", null ],
    [ "PIN_RFM95_DIO0", "_hardware_definition_8h.html#a5d7faebc2e234a31f594f272886d5759", null ],
    [ "PIN_RFM95_RESET", "_hardware_definition_8h.html#a314dfcd705ed590ca54b2c8dbb67274e", null ],
    [ "PIN_RTC_CS", "_hardware_definition_8h.html#aa801e7c2258fb6ad1e3d0a0df204ffd7", null ],
    [ "PIN_RTC_INT", "_hardware_definition_8h.html#ad4c30bf2f7ba49850b4c60af34abf820", null ],
    [ "PIN_STATUS_LED", "_hardware_definition_8h.html#a9cccc3fd0833b8971461317cc3750096", null ],
    [ "SPI_HW_SPEED", "_hardware_definition_8h.html#a704ba7e83ec04519c2b2c81be47443f9", null ]
];