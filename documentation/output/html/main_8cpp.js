var main_8cpp =
[
    [ "COMPUTER_BAUDRATE", "main_8cpp.html#abf0f581a4c0f1023eacfe1fa159d9c8a", null ],
    [ "E_SENSOR_BAUDRATE", "main_8cpp.html#a02a4a7318019b1aa19380fc7503b6b60", null ],
    [ "MAX_FORWARING_LEN", "main_8cpp.html#a08f3995a08ee055e459f754dd3c1eedf", null ],
    [ "NULL_SERIAL_NUMBER", "main_8cpp.html#af0de465a707e439913289f2e453de92e", null ],
    [ "WAIT_FOR_USB_TIME", "main_8cpp.html#a79e1e9b6f8093b2a68a5d16d93df5d66", null ],
    [ "State", "main_8cpp.html#ae9c42c594bb848c81ace2ff29f64dc89", [
      [ "WaitForWelcome", "main_8cpp.html#ae9c42c594bb848c81ace2ff29f64dc89a1c611473aea0a11382bbd63c7eb0d454", null ],
      [ "Interrupted", "main_8cpp.html#ae9c42c594bb848c81ace2ff29f64dc89acd6a11da1266a27d33c667b4ba0b07ef", null ],
      [ "TransmitData", "main_8cpp.html#ae9c42c594bb848c81ace2ff29f64dc89a0ad41580d8e1cff8c915ccc77fc60f31", null ],
      [ "ReceiveAdjustParameter", "main_8cpp.html#ae9c42c594bb848c81ace2ff29f64dc89a870afe4af0f55813bcc92fb11e58db64", null ]
    ] ],
    [ "loop", "main_8cpp.html#afe461d27b9c48d5921c00d521181f12f", null ],
    [ "set_state", "main_8cpp.html#abef3f51e03a484281acce64e7d912c21", null ],
    [ "setup", "main_8cpp.html#a4fc01d736fe50cf5b977f755b675f11d", null ],
    [ "_DEBUG_SERIAL_ESTABLISHED", "main_8cpp.html#acef842d1c6baf01db8e5b1d0abba43d7", null ],
    [ "curr_state", "main_8cpp.html#a0688506fb04841a594bcbe324d5c2e7b", null ],
    [ "debug_interface", "main_8cpp.html#abbe08c721bc2d840ff49f49e3e63f9cf", null ],
    [ "external_adc", "main_8cpp.html#aa128233cac94098a39bd9ee84ea2fb61", null ],
    [ "external_rtc", "main_8cpp.html#a0a2ba1ad979d0537bfcd9c6b14196bd4", null ],
    [ "external_sensor", "main_8cpp.html#a9e994e4423e111a6cbd41a4b9487402a", null ],
    [ "hub_mesh_id", "main_8cpp.html#a03d7afea9ab38cfe556ee675bdfcbeab", null ],
    [ "last_type", "main_8cpp.html#a26ededc81b8eed3c34350df7d165cdba", null ],
    [ "remaining_time", "main_8cpp.html#a6d8236c9534f2c98275ba12f9b4ca2e4", null ],
    [ "status_led", "main_8cpp.html#a406fc1f1307853ef916c16e58c614023", null ],
    [ "stored_data", "main_8cpp.html#a5c67ce1dede3f2911ffc9c0fb470518a", null ],
    [ "wireless_radio", "main_8cpp.html#a32d62a5aaad622f841df6698e6d694f3", null ]
];