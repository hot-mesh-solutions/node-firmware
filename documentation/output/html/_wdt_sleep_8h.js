var _wdt_sleep_8h =
[
    [ "WDT_1S", "_wdt_sleep_8h.html#ac903de498f76d241b323f96fedbb622d", null ],
    [ "WDT_2S", "_wdt_sleep_8h.html#ab0ba6e85857856de5a8cabb4364f6289", null ],
    [ "WDT_4S", "_wdt_sleep_8h.html#ace206303b3e1d9821500c26e1c15fb21", null ],
    [ "WDT_500MS", "_wdt_sleep_8h.html#a930e8dc396ad9d7f980b99cfe7ab4886", null ],
    [ "WDT_8S", "_wdt_sleep_8h.html#a8bf94bf468bc4bb419020427ad7e9cdb", null ],
    [ "change_wdt_sleep_time", "_wdt_sleep_8h.html#ac236d6588dc2172a9c8aabfc1af83f1b", null ],
    [ "go_to_sleep", "_wdt_sleep_8h.html#aef74ebaa0dbe2080a85e555b12deee54", null ],
    [ "wdt_sleep_for", "_wdt_sleep_8h.html#a99e50e6e8f22a8ab3d5306241d0dd043", null ]
];