var helpers_8h =
[
    [ "INIT_MSG_FAILED", "helpers_8h.html#ac9cc4d51b539f59dedee1d57efcac3e9", null ],
    [ "INIT_MSG_SUCCESS", "helpers_8h.html#a8976c6c6e2d646b05d0dc384ef7153c7", null ],
    [ "between", "helpers_8h.html#acd3fdfe26db7733f127289e0ac8ed71a", null ],
    [ "bytes_to_float", "helpers_8h.html#a732aee528d9a059218cc6bc8984fc0fc", null ],
    [ "bytes_to_time", "helpers_8h.html#a3ff1f229bccff1f1b5c5a0ae5747da84", null ],
    [ "do_nothing", "helpers_8h.html#a15196b401013e8f2274566f6432b4ba4", null ],
    [ "float_to_uint8", "helpers_8h.html#a84dff462167c4120c836b089a1fa35e3", null ],
    [ "flush_serial_buffer", "helpers_8h.html#a6f232b525a2bd026aec73549c14157eb", null ],
    [ "mapf", "helpers_8h.html#abe98bb53469003f2f4db7a06af2f944c", null ],
    [ "time_to_uint8", "helpers_8h.html#a83779afe3b032d8d31deca1ccca7ef81", null ],
    [ "uint16_to_uint8", "helpers_8h.html#a50108b7b2f64e1e5c7d33a430bc00780", null ],
    [ "uint8_to_float", "helpers_8h.html#aff1c5d92219fd85481923605c4612d80", null ],
    [ "uint8_to_time", "helpers_8h.html#abee1a2e1b17b516f1058538c63ec79bd", null ],
    [ "uint8_to_uint16", "helpers_8h.html#a5d164695ed10d157ff44318f2e6717f3", null ]
];