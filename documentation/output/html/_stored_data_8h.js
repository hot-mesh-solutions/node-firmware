var _stored_data_8h =
[
    [ "SettingsStruct", "struct_settings_struct.html", "struct_settings_struct" ],
    [ "SensordataStruct", "struct_sensordata_struct.html", "struct_sensordata_struct" ],
    [ "SettingsToByteArr", "union_settings_to_byte_arr.html", "union_settings_to_byte_arr" ],
    [ "StoredData", "class_stored_data.html", "class_stored_data" ],
    [ "ACTUAL_SETTINGS_END_ADDR", "_stored_data_8h.html#a71a83a47e7c9574b2291d19a86075814", null ],
    [ "ACTUAL_SETTINGS_START_ADDR", "_stored_data_8h.html#afc037505b25d8681734ac720a39bcaaa", null ],
    [ "DEFAULT_HUB_MESH_ID", "_stored_data_8h.html#af4ca8a57f053c7ee4b0e59ee20d6ad5a", null ],
    [ "DEFAULT_SETTINGS_END_ADDR", "_stored_data_8h.html#ab4153f5abe250d94b6d087b8ea9a3afa", null ],
    [ "DEFAULT_SETTINGS_START_ADDR", "_stored_data_8h.html#a12b2772807cf8270fec8c08133ed0eec", null ],
    [ "SENSORDATA_END_ADDR", "_stored_data_8h.html#a2242def61dee465e8229094072d34084", null ],
    [ "SENSORDATA_START_ADDR", "_stored_data_8h.html#a3106e2a19e7835bf8f85e1746ce063ca", null ]
];