var _external_adc_8h =
[
    [ "ExternalAdc", "class_external_adc.html", "class_external_adc" ],
    [ "_3V3_DIVIDER", "_external_adc_8h.html#a7015989a30104f15980684741cb89468", null ],
    [ "_3V3_SUPPLY", "_external_adc_8h.html#a54c87bc33c407577e4aa7f1f6ee28f08", null ],
    [ "_5V_DIVIDER", "_external_adc_8h.html#afe29d217ada7baf87ac036e4788fc549", null ],
    [ "_5V_SUPPLY", "_external_adc_8h.html#ac96494dcb97bde1cba69b2a6d09a681c", null ],
    [ "BATT_DIVIDER", "_external_adc_8h.html#ae5dbd94aa6855b7a35d908eaeff90206", null ],
    [ "EXT_CM_MULTIPLIER", "_external_adc_8h.html#a0adad9434e1e24a521c08c065c64b419", null ],
    [ "INTERNAL_1V1_REFERENCE_CONSTANT", "_external_adc_8h.html#aaee48e96d4082a9b7bdaae7b6df4330f", null ],
    [ "MAIN_CM_MULTIPLIER", "_external_adc_8h.html#af00fb3eef8d2e1a0e1212bdb24b90ad4", null ],
    [ "MAX_ADC_REFERENCE", "_external_adc_8h.html#a051997eb335e6e75e4d5966b0225f024", null ],
    [ "MAX_ADC_RESOLUTION", "_external_adc_8h.html#ae97022eba2973b4d687f7b28aeab233a", null ],
    [ "MAX_SOLAR_VOLTAGE", "_external_adc_8h.html#a6195e9cc77b693ee7e916172478d713b", null ],
    [ "SOLAR_DIVIDER", "_external_adc_8h.html#a1df2e11aea0c4a172a131fdc14e46dbe", null ],
    [ "USE_INTERNAL", "_external_adc_8h.html#aed483d7d0974650e816be9fcea9a93a2", null ]
];