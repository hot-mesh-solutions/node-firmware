var class_external_adc =
[
    [ "initialize", "class_external_adc.html#a37af4ef2e1b0f306f8a796a25798a514", null ],
    [ "is_initialized", "class_external_adc.html#ac50960a1e03f0fbda872fb32c3806c0d", null ],
    [ "read_3v3_supply", "class_external_adc.html#a31199ff3ecb7703490b6e3885a964eae", null ],
    [ "read_5v_supply", "class_external_adc.html#ac0c856d8694584c168a402da0d9f02d8", null ],
    [ "read_battery", "class_external_adc.html#a0aec54af31738cf2dff21f7d77b2cfcb", null ],
    [ "read_channel", "class_external_adc.html#aca9d4eaa8957e98ee7c4a7bde9fec144", null ],
    [ "read_external_current", "class_external_adc.html#aefb0c8acd57cbfc22fb67210fb24f080", null ],
    [ "read_main_current", "class_external_adc.html#abbd8b6769b53f323b91115faca85de95", null ],
    [ "read_solar", "class_external_adc.html#affe9c7650c104dde2e9f57557d98ed4e", null ],
    [ "read_vref", "class_external_adc.html#a375ff5db323f494f0ce88cf5528dc017", null ]
];