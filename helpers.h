#ifndef NODE_FIRMWARE_HELPERS_H
#define NODE_FIRMWARE_HELPERS_H

#include <Arduino.h>
#include <HardwareSerial.h>
#include <Time.h>
#include "DebugSerial.h"

#define INIT_MSG_SUCCESS (F("Success"))
#define INIT_MSG_FAILED (F("Failed"))

float mapf(float, float, float, float, float);
uint8_t float_to_uint8(float, uint8_t);
float uint8_to_float(uint8_t, uint8_t, uint8_t, uint8_t);
uint8_t time_to_uint8(time_t, uint8_t);
time_t uint8_to_time(uint8_t, uint8_t, uint8_t, uint8_t);
uint8_t uint16_to_uint8(uint16_t, bool);
uint16_t uint8_to_uint16(uint8_t, uint8_t);
float bytes_to_float(uint8_t *);
time_t bytes_to_time(uint8_t *);
void flush_serial_buffer();
bool between(long, long, long);
void do_nothing();

#endif //NODE_FIRMWARE_HELPERS_H
