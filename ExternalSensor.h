#ifndef NODE_FIRMWARE_EXTERNALSENSOR_H
#define NODE_FIRMWARE_EXTERNALSENSOR_H

#include <Arduino.h>
#include <HardwareSerial.h>
#include <Time.h>

#include "DebugSerial.h"
#include "HardwareDefinition.h"
#include "helpers.h"

#define SENSOR_WAIT_ATTEMPTS (30)
#define SENSOR_WAIT_DELAY (100)

enum SensorType : uint8_t {
    BATTERY_VOLTAGE     = 0x01,
    SOLAR_VOLTAGE       = 0x02,
    TEMPERATURE         = 0x03
};

class ExternalSensor {
public:
    ExternalSensor(long);
    void set_power(bool);
    float check_sensor();
    bool is_powered();

private:
    long baudrate;
    bool _is_powered;
};

#endif //NODE_FIRMWARE_EXTERNALSENSOR_H
